# -*- coding: utf-8 -*-
import sys
from cls.word_pick import *
from cls.testExecutor import *
from cls.testProcessor import *
from pyvirtualdisplay import Display
import uuid
import multiprocessing
import threading
from threading import Thread
import Queue 


q = Queue.Queue()
running = 0

class tester(threading.Thread):
    def __init__(self, host, param, user, lang):
        threading.Thread.__init__(self)
        self.host = host
        self.param = param
        self.user = user 
        self.lang = lang
        self.test_worker = testExecutor(self.host, self.param, self.user, self.lang)
        global running
        running += 1
        print running

    def run(self):
        try:
            print 'starting', self.getName()
            self.test_worker.test_execute()
            global running
            running -= 1
            print 'ended', self.getName()
        except:
            global running
            running -= 1
            print 'failed', self.getName()

host = sys.argv[1]
actions = sys.argv[2]
users = sys.argv[3]
lng = sys.argv[4]
try:
    max_thread = sys.argv[5]
except IndexError:
    max_thread = 4
  
    


if '@' in actions:
    inurl = sys.argv[5]
    ssid = sys.argv[6]
else:
    inurl = None
    ssid = None


tx = word_pick(txt_mod = actions)

if 'X' in sys.argv[2]:
    display = Display(visible=0, size=(1920, 1080))
    display.start()

strtid = ' [' + str(uuid.uuid4())[:8] +'] '

friday = testExecutor(host, actions, users, lng)

friday.test_execute()

if "!" in actions:
    act = actions.replace("Y","").replace('j','')
    if 'X' not in act:
        act += 'X'
    act += '~'
    print friday.langs
    print friday.users
    jobs = []
    browser = ['G', 'F', 'P']
    for user in friday.users:
        print(running)
        print(user)
        usr = user
        act = act.replace('G','').replace('F','').replace('P','')
        if friday.usr_pass == "pass_set":
            pwd = friday.pwds[user.split('@')[0]]
            print('pwd:' + pwd )
        else:
            pwd = friday.usr_pass
            print('pwd:' + pwd )
        lstatt = []
        for u in friday.users:
            if usr != u:
                lstatt.append(u)    
        prm = usr + ":" +  pwd + ";" + ",".join(lstatt)
        lang = random.choice(friday.langs)
        act += random.choice(browser)
        todo = [host, act, prm, lang]
        q.put(todo) 

   
    while not q.empty():
        if running < int(max_thread):
            work = q.get()
            print "run " + str(running)
            print "job " + str(len(jobs))
            me = tester(work[0], work[1], work[2], work[3].encode('utf-8'))
            me.start()
            jobs.append(me)
            print "jobs to do " + str(q.qsize())
            
        else:
            time.sleep(1)
    

    if running < 1 and 'X' in sys.argv[2]:
        display.stop()