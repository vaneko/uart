from locatEle import *


class imChecker(locatEle):
    def __init__(self, driver):
        self.d = driver

    def set_online(self):
        if 'W' in self.act or float(str(self.vers)[:4]) >= 12.0:
            im_stat = self.snd_cli("I", "gui.frm_main.stat/5", 5)
        else:
            im_stat = self.snd_cli("I", "gui.frm_main.stat/3", 5 )
        self.snd_cli("C", ".ico.ico2.online", 5)

    def check_im(self):
        if 'W' in self.act or float(str(self.vers)[:4]) >= 12.0:
            im_stat = self.get_ele("I", "gui.frm_main.stat/5", "state", 5 )
        else:
            im_stat = self.get_ele("I", "gui.frm_main.stat/3", "state", 5 )
            
        if im_stat == "offline":        
            self.logger.error(self.msid + "IM STATUS OFFLINE")
            self.set_online()
        else:
            self.logger.info(self.msid + "IM STATUS " + str(im_stat))


