import smtplib
import os
from loginUrl import *
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from setupDriver import *

class reporter(setupDriver):
    """ run after take_pic """
    def __init__(self, msid, exec_typ, **kwargs):
        self.msid = msid
        self.status = exec_typ
        self.surname = 'Woodward'
        for key, value in kwargs.items():
            setattr(self, key, value)

    def readLog(self):
        self.log = []
        with open('autotest.log') as f:
            for line in f:
                if self.msid in line:
                    self.log.append(line)
            return self.log

    def SendMail(self, host):
        self.host = host
        self.status += 'FAIL' 
        self.ImgFileName = os.path.abspath(self.host + self.msid + '.png')
        
        
        img_data = open(self.ImgFileName, 'rb').read()
        msg = MIMEMultipart()
        msg['Subject'] = self.host + self.msid + self.status
        msg['From'] = self.smtp_from
        msg['To'] = self.rcpt_to

        txt = self.readLog()
        text = ""
        for i in txt:
            text += i+'\n'
        msbody = MIMEText(text)
        msg.attach(msbody)
        image = MIMEImage(img_data, name='error.png')
        msg.attach(image)
         

        try:
            s = smtplib.SMTP(self.smtp_server, self.smtp_port)
            s.ehlo()
            s.starttls()
            s.ehlo()
            s.login(self.smtp_from, self.smtp_pass)
            s.sendmail(self.smtp_from, self.rcpt_to, msg.as_string())
            s.quit()
        except:
            e = sys.exc_info()

    def send_report(self, host, env, times, **kwargs):
        self.host = host
        self.times = times
        self.env = env
        self.status += 'OK'
        
        msg = MIMEMultipart()
        msg['Subject'] = self.host + self.msid + self.status
        msg['From'] = self.smtp_from
        msg['To'] = self.rcpt_to

        text = "[TEST REPORT]:\n\n"
        text += "Environment:\n\n"
        for k, v in sorted(self.env.items()):
            text += str(k) + "      " + str(v) + '\n\n'


        text += "\n\nExecution Time\n\n"
        for k, v in sorted(self.times.items()):
            text += str(k) + "      " + str(v) + '\n\n'
        
        text += "\n\nOverall Time\n\n"
        for k, v in sorted(kwargs.items()):
            if v != None:
                text += str(k) + ': ' + str(v) + '\n\n'

        msbody = MIMEText(text)
        msg.attach(msbody)

        
        try:
            s = smtplib.SMTP(self.smtp_server, self.smtp_port)
            s.ehlo()
            s.starttls()
            s.ehlo()
            s.login(self.smtp_from, self.smtp_pass)
            s.sendmail(self.smtp_from, self.rcpt_to, msg.as_string())
            s.quit()
        except:
            e = sys.exc_info()
            print(e)
            #self.logger.error(self.msid + str(e))