from locatEle import *

class compButtons(locatEle):

    def __init__(self, driver):
        self.d = driver
        self.btnerr = 0


    def press_ok(self, strn, **kwargs):
        locals()['mod'] = 2
        for key, value in kwargs.items():
            locals()[key] = value
        #time adjusted temp
        self.snd_cli("I", "gui." + strn + ".x_btn_ok#main", 3 * self.tp.retime(2), locals()['mod'])
        if self.error == 0:
            self.btnerr = 0
            self.logger.debug(self.msid + "OK pressed")
        else:
            self.btnerr = 1
            self.snd_cli("I", "gui." + strn + ".x_btn_cancel#main", 3 * self.tp.retime(1), mod = 1)
            self.logger.debug(self.msid + "CANCEL pressed")
            if self.error == 0:
                self.snd_cli("I", "gui.frm_confirm.x_btn_ok#main", 3 * self.tp.retime(1), mod = 1)
                self.logger.debug(self.msid + "OK pressed")
            else:
                self.logger.error(self.msid + "FATAL ERROR")
                
    #@retry(stop_max_attempt_number=5, wait_exponential_max=5000 )
    def press_x(self, strn, **kwargs):
        count = 0
        for key, value in kwargs.items():
            locals()[key] = value
        while self.ret_ele("I", "gui."+ strn +"#rem", 3, self.tp.retime(2)) == 'True':
            time.sleep(1)
            if count < 5:
                if 'mod' in kwargs:
                    self.snd_cli("I", "gui." + strn + "#rem", 3 * self.tp.retime(2), mod = locals()['mod'] )
                else:
                    self.snd_cli("I", "gui." + strn + "#rem", 3 * self.tp.retime(2))
                if self.error == 0:
                    self.btnerr = 0
                else:
                    self.btnerr = 1
                count = (( count + 1 ))
                self.logger.debug(self.msid + "X pressed %s times" % count)
        if self.ret_ele("I", "gui."+ strn +"#rem", 3, self.tp.retime(0)) == 'False' :
            self.btnerr = 0


    def press_cancel(self, strn):
        self.snd_cli("I", "gui." + strn + ".x_btn_cancel#main", 3 * self.tp.retime(2), mod = 1)
        if self.error == 0:
            self.btnerr = 0
        else:
            self.btnerr = 1
        self.logger.debug(self.msid + "CANCEL pressed")

    def refresh(self, strn):
        self.snd_cli("I", "gui." + strn + ".x_btn_refresh#main", 3 * self.tp.retime(2), mod = 1)
        if self.error == 0:
            self.btnerr = 0
        else:
            self.btnerr = 1
        self.logger.debug(self.msid + "REFRESH pressed")
    
    def msg_options(self, strn):
        self.snd_cli("I", "gui." + strn + ".x_btn_options", 5 * self.tp.retime(1))
        if self.error == 0:
            self.btnerr = 0
        else:
            self.btnerr = 1