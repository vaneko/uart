from locatEle import *
from reporter import *
import time
from word_pick import *
from setupDriver import *

class webAdmin(locatEle):
    """"WAD basic operations"""

    def wad_start(self, url, uname, upass, **kwargs):
        self.hostname = None
        for key, value in kwargs.items():
            setattr(self, key, value)  
            
        self.url = url
        self.uname = uname
        self.upass = upass
        
        try:
            self.login(self.url)
            self.logger.debug(self.msid + self.get_ele('X', '//*[@id="content"]/div/section[3]/p', 'title', 10))
            if self.lang != None:
                self.fetch_lang(self.lang)
                self.logger.debug(self.msid + "language(s) fetched")
            self.logger.debug(self.msid + "entering username")
            time.sleep(0.1)
            self.snd_key("N", "email-address", self.uname, 5)
            self.snd_cli("N", "next", 5)
            self.logger.debug(self.msid + "entering password")
            self.snd_key("N", "password", self.upass, 5)
            self.snd_cli("N", "next", 5)
            self.logger.debug(self.msid + "login proceed")
        except:
            self.take_pic(self.hostname)
            if '^' in self.act:
                typ = ' NB '
            else:
                typ = 'H'
            bob = reporter(self.msid, typ, self.hostname, self.env, self.report, test_duration = exe_tim, attach_max_time = mxt, attach_min_time = mnt, attach_avrg_time = avrg)
            bob.SendMail(self.hostname)
    

    def set_api(self, param):
        self.param = param
        self.logger.debug(self.msid + "setting api: " + self.param)
        self.snd_cli("I", "gui.frm_main.btn_menu_main#main", 5)
        self.snd_cli("X", "//*[starts-with(@id, 'gui.frm_main.aside_main_menu#tabmenu-api_console')]", 5)
        self.ap_var, self.ap_val = self.param.split('::')
        self.snd_key("I", "gui.popup.main.input_search#main", self.ap_var + Keys.RETURN, 5)
        self.logger.debug(self.msid + 'searched for API: ' + self.ap_var)
        self.val_ele = str('//*[@id="gui.popup.main.consoledialog.list.obj_' + self.ap_var + '#main"]')
        self.snd_key("X", self.val_ele, self.ap_val, 5, clear='yes')
        self.logger.debug(self.msid + 'set api ' + self.ap_var + ':' + self.ap_val)
        self.sav_ele = str('gui.popup.main.consoledialog.list.obj_' + self.ap_var + '_save#main')
        self.snd_cli("I", self.sav_ele, 5)
        self.logger.debug(self.msid + "api saved")
        self.snd_cli("I", "gui.popup.main.btn_close_search#main", 5)
        self.logger.debug(self.msid + 'api ended')




    def creat_user(self):
        self.logger.debug(self.msid + "user mode")
        time.sleep(0.1)
        self.snd_cli("I", "gui.frm_main.btn_menu_add#main", 10)
        self.logger.debug(self.msid + "adding new user")
        time.sleep(0.1)
        self.snd_cli("I", "gui.frm_main.aside_menu#tabmenu-new_user", 10)
    
    
    def ecce_homo(self, uname, upass, save):
        self.logger.debug(self.msid + "creating user")
        #create user dialog
        self.uname = uname
        self.upass = upass
        self.save = save
        self.sur_name = word_pick(text = self.lng, txt_mod = self.act).gime_som()
        time.sleep(0.1)
        self.snd_key("I", "gui.popup.main.newaccount.input_name#main", self.uname, 15)
        time.sleep(0.1)
        self.snd_key("I", "gui.popup.main.newaccount.input_surname#main", self.sur_name, 1)
        time.sleep(0.1)
        self.snd_key("I", "gui.popup.main.newaccount.input_alias#main", self.uname, 1)
        time.sleep(0.1)
        self.snd_key("I", "gui.popup.main.newaccount.input_password#main", self.upass, 1, clear='yes')
        time.sleep(0.1)
        self.snd_cli("I", "gui.popup.main.btn_" + self.save + "#main", 1)
        self.logger.debug(self.msid + "created user: " + self.uname + " " + self.sur_name)
        time.sleep(1)

    def logout(self):
        time.sleep(0.1)
        self.snd_cli("C", "i.icon-dropdown-arrow.usermenu__icon", 5)
        self.snd_cli("I", "gui.frm_main.user_menu#tabmenu-logout", 5)
