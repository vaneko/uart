import logging
import sys
import logging
from logging import DEBUG, INFO, ERROR

class Logger(object):
    def __init__(self, name, format="%(asctime)s | %(levelname)s | %(message)s", level=DEBUG):
        # Initial construct.
        self.format = format
        self.level = level
        self.name = name

        # Logger configuration.
        self.console_formatter = logging.Formatter(self.format)
        self.console_logger = logging.FileHandler('autotest.log', mode='a', encoding='utf-8')
        self.console_logger.setFormatter(self.console_formatter)
        self.loggers = {}
        # Complete logging config.
        if self.loggers.get(name):
            self.logger = self.loggers.get(name)
        else:
            self.logger = logging.getLogger(name)
            self.logger.setLevel(self.level)
            self.logger.addHandler(self.console_logger)
            self.loggers.update(dict(name=self.logger))

    def info(self, msg, extra=None):
        try:
            msg = msg.decode('UTF-8')
        except (UnicodeEncodeError):
            msg = msg.encode('UTF-8')

        self.logger.info(msg, extra=extra)

    def error(self, msg, extra=None):
        try:
            msg = msg.decode('UTF-8')
        except (UnicodeEncodeError):
            msg = msg.encode('UTF-8')
        
        self.logger.error(msg, extra=extra)

    def debug(self, msg, extra=None):
        try:
            msg = msg.decode('UTF-8')
        except (UnicodeEncodeError):
            msg = msg.encode('UTF-8')
        self.logger.debug(msg, extra=extra)


    def warn(self, msg, extra=None):
        try:
            msg = msg.decode('UTF-8')
        except (UnicodeEncodeError):
            msg = msg.encode('UTF-8')
        self.logger.warn(msg, extra=extra)

    def exception(self, msg, extra=None):
        try:
            msg = msg.decode('UTF-8')
        except (UnicodeEncodeError):
            msg = msg.encode('UTF-8')
        self.logger.exception(msg, extra=extra)

    def get_logger(self):
        return self.logger
