from loginUrl import *
from reporter import *
from setupDriver import *
from timeOut import *
import os

class locatEle(loginUrl):
    """locate elements, clicks and send keys"""

    def retry_ifno_element(result):
        return len(str(result)) <= 0
        
        
    def paparazzi_style(self, elemname, **kwargs):
        """ mod <= 0 = failure, 1 = report, 2 = bug, ! mod + X = exit """
        for key, value in kwargs.items():
            locals()[key] = value
        self.take_pic(self.hostname)
        if '^' in self.act:
            typ = ' NB '
        else:
            typ = 'H'
        self.logger.debug(self.msid + "failed id: " + elemname )
        if 'mod' in kwargs:
            if locals()['mod'] == 1:
                self.logger.debug(self.msid + "Possible missconfiguration or reported BUG")
                bob = reporter(self.msid, typ, smtp_from = self.conf.smtp_from, smtp_pass = self.conf.smtp_pass, rcpt_to = self.conf.rcpt_to, smtp_server = self.conf.smtp_server, smtp_port = self.conf.smtp_port)
                bob.SendMail(self.hostname)            
            elif locals()['mod'] == 0:
                self.logger.debug(self.msid + "Possible missconfiguration or reported BUG")
                
            else:
                self.logger.error(self.msid + "a BUG! it's a BUG!!!")
                bob = reporter(self.msid, typ, smtp_from = self.conf.smtp_from, smtp_pass = self.conf.smtp_pass, rcpt_to = self.conf.rcpt_to, smtp_server = self.conf.smtp_server, smtp_port = self.conf.smtp_port)
                bob.SendMail(self.hostname)
        self.logger.debug(self.msid + "incident has been reported")
        if 'mod' in kwargs:
            if locals()['mod'] == 0 or locals()['mod'] == 1:
                self.logger.debug(self.msid + "Acceptable failure")
            elif 'X' in self.act or 'r' in self.act:
                #time.sleep(1)
                self.logger.debug(self.msid + "terminating connection")
                try:
                    self.quiBro()
                except:
                    pass
                return 5
        elif 'X' in self.act or 'r' in self.act:
            #time.sleep(1)
            self.logger.debug(self.msid + "terminating connection")
            try:
                self.quiBro()
            except:
                pass
            return 5
            

    def loc_method(self, id, **kwargs):
        for key, value in kwargs.items():
            locals()[key] = value
        methods = { 'N':self.d.find_elements_by_name, 'n':self.d.find_element_by_name, 'I':self.d.find_elements_by_id, 'i':self.d.find_element_by_id, 'C':self.d.find_elements_by_css_selector, 'c':self.d.find_element_by_css_selector, 'CN':self.d.find_elements_by_class_name, 'cn':self.d.find_element_by_class_name, 'X':self.d.find_elements_by_xpath, 'x':self.d.find_element_by_xpath }
        return methods[id]
        

    def send_file(self, target, pathtofile):
        droparea = self.d.find_element_by_id(target)
        droparea.send_keys(pathtofile)


    def snd_key(self, typ, name, txt, wait_time, mod = None,  **kwargs):
        try:
            txt = str(txt).encode('utf-8')
        except (UnicodeDecodeError):
            txt = str(txt).decode('utf-8')
        for key, value in kwargs.items():
            locals()[key] = value
        try:
            if typ == "N":
                element = WebDriverWait(self.d, wait_time).until(
                    EC.element_to_be_clickable((By.NAME, name ))
                )
                if 'clear' in kwargs:
                    element.clear()
                element.send_keys(txt)
                if 'retr' in kwargs:
                    element.send_keys(Keys.RETURN)
            elif typ == "I":
                element = WebDriverWait(self.d, wait_time).until(
                    EC.element_to_be_clickable((By.ID, name ))
                )
                if 'clear' in kwargs:
                    element.clear()
                element.send_keys(txt)
                if 'retr' in kwargs:
                    element.send_keys(Keys.RETURN)
                    
            elif typ == "C":
                element = WebDriverWait(self.d, wait_time).until(
                    EC.element_to_be_clickable((By.CSS_SELECTOR, name ))
                )
                if 'clear' in kwargs:
                    element.clear()
                element.send_keys(txt)
                if 'retr' in kwargs:
                    element.send_keys(Keys.RETURN)
            elif typ == "CN":
                element = WebDriverWait(self.d, wait_time).until(
                    EC.element_to_be_clickable((By.CLASS_NAME, name ))
                )
                if 'clear' in kwargs:
                    element.clear()
                element.send_keys(txt)
                if 'retr' in kwargs:
                    element.send_keys(Keys.RETURN)
            elif typ == "X":
                element = WebDriverWait(self.d, wait_time).until(
                    EC.element_to_be_clickable((By.XPATH, name ))
                )
                if 'clear' in kwargs:
                    element.clear()
                element.send_keys(txt)
                if 'retr' in kwargs:
                    element.send_keys(Keys.RETURN)
        except:
            e = sys.exc_info()
            self.logger.error(self.msid + str(e))
            self.paparazzi_style(name, mod = locals()['mod'])

    def snd_keybr(self, element, keyboard, **kwargs):
        try:
            if keyboard == 'clear':
                element.clear()
            else:
                element.send_keys(keyboard)
        except:
            e = sys.exc_info()
            self.logger.error(self.msid + str(e))
            self.paparazzi_style(clmn)
                    
    #click on element
    def snd_cli(self, typ, name, wait_time, mod = None, **kwargs):
        self.error = 0
        for key, value in kwargs.items():
            locals()[key] = value
        try:
            if typ == "N":
                element = WebDriverWait(self.d, wait_time).until(
                    EC.element_to_be_clickable((By.NAME, name ))
                )
                element.click()
            elif typ == "I":
                element = WebDriverWait(self.d, wait_time).until(
                    EC.element_to_be_clickable((By.ID, name ))
                )
                element.click()
            elif typ == "C":
                element = WebDriverWait(self.d, wait_time).until(
                    EC.element_to_be_clickable((By.CSS_SELECTOR, name ))
                )
                element.click()
            elif typ == "CN":
                element = WebDriverWait(self.d, wait_time).until(
                    EC.presence_of_element_located((By.CLASS_NAME, name ))
                )
                element.click()
            elif typ == "X":
                element = WebDriverWait(self.d, wait_time).until(
                    EC.element_to_be_clickable((By.XPATH, name ))
                )
                element.click()
        except:
            e = sys.exc_info()
            self.logger.error(self.msid + str(e))
            self.paparazzi_style(name, mod = locals()['mod'])
            self.error = 1

    def js_hoover(self):
        self.d.execute_script("document.getElementById('gui.frm_main.hmenu1').dispatchEvent(new CustomEvent('mousemove', {detail: document.querySelector('.ico.ico_new.nodes.end span')}))")
    
    #get attribute of element
    def get_ele(self, typ, name, txt, wait_time):
        try:
            if typ == "N":
                element = WebDriverWait(self.d, wait_time).until(
                    EC.presence_of_element_located((By.NAME, name ))
                )
                return element.get_attribute(txt)
            elif typ == "I":
                element = WebDriverWait(self.d, wait_time).until(
                    EC.presence_of_element_located((By.ID, name ))
                )
                return element.get_attribute(txt)
            elif typ == "C":
                element = WebDriverWait(self.d, wait_time).until(
                    EC.presence_of_element_located((By.CSS_SELECTOR, name ))
                )
                return element.get_attribute(txt)
            elif typ == "CN":
                element = WebDriverWait(self.d, wait_time).until(
                    EC.presence_of_element_located((By.CLASS_NAME, name ))
                )
                return element.get_attribute(txt)
            elif typ == "X":
                element = WebDriverWait(self.d, wait_time).until(
                    EC.presence_of_element_located((By.XPATH, name ))
                )
                return element.get_attribute(txt)
        except:
            e = sys.exc_info()
            self.logger.error(self.msid + str(e))
            self.paparazzi_style(name)        

    
            
    @retry(retry_on_result=retry_ifno_element, stop_max_delay=10000) #retry_on_result=retry_ifno_element, 
    def return_multi_elem(self, typ1, name1, typ2, name2, wait_time, **kwargs):
        """ mod 1 return True """
        for key,value in kwargs.items():
            locals()[key]=value
        method1 = self.loc_method(typ1)
        method2 = self.loc_method(typ2)
        try:
            element = WebDriverWait(self.d, wait_time).until(
                lambda driver : method1(name1) or method2(name2)
            )
            return element
        except:
            return 'E'

    @retry(retry_on_result=retry_ifno_element, stop_max_attempt_number=5)
    def ret_ele(self, typ, name, mod, wait_time, **kwargs):
        """ mod returns: 1,3 = true, 0,2 = elemnent """
        try:
            if typ == "N":
                if mod == 0 or mod == 1:
                    element = WebDriverWait(self.d,wait_time).until(
                        EC.presence_of_element_located((By.NAME, name ))
                    )
                elif mod == 2:
                    element = WebDriverWait(self.d,wait_time).until(
                        EC.presence_of_elements_located((By.NAME, name ))
                    )
                elif mod == 3:
                    element = WebDriverWait(self.d, wait_time).until(
                        EC.element_to_be_clickable((By.NAME, name ))
                    )
                if mod == 1 or mod == 3:
                    return "True"
                elif mod == 0 or mod == 2:
                    return element
            elif typ == "I":
                if mod == 0 or mod == 1:
                    element = WebDriverWait(self.d, wait_time).until(
                        EC.presence_of_element_located((By.ID, name ))
                    )
                elif mod == 2:
                    element = WebDriverWait(self.d, wait_time).until(
                        EC.presence_of_elements_located((By.ID, name ))
                    )
                elif mod == 3:
                    element = WebDriverWait(self.d, wait_time).until(
                        EC.visibility_of_element_located((By.ID, name ))
                    )
                if mod == 1 or mod == 3:
                    return "True"
                elif mod == 0 or mod == 2:
                    return element
            elif typ == "C":
                if mod == 0 or mod == 1:
                    element = WebDriverWait(self.d, wait_time).until(
                        EC.presence_of_element_located((By.CSS_SELECTOR, name ))
                    )
                elif mod == 2:
                    element = WebDriverWait(self.d, wait_time).until(
                        EC.presence_of_elements_located((By.CSS_SELECTOR, name ))
                    )
                elif mod == 3:
                    element = WebDriverWait(self.d, wait_time).until(
                        EC.element_to_be_clickable((By.CSS_SELECTOR, name ))
                    )
                if mod == 1 or mod == 3:
                    return "True"
                elif mod == 0 or mod == 2:
                    return element
            elif typ == "CN":
                if mod == 0 or mod == 1:
                    element = WebDriverWait(self.d, wait_time).until(
                        EC.presence_of_element_located((By.CLASS_NAME, name ))
                    )
                elif mod == 2:
                    element = WebDriverWait(self.d, wait_time).until(
                        EC.presence_of_elements_located((By.CLASS_NAME, name ))
                    )
                elif mod == 3:
                    element = WebDriverWait(self.d, wait_time).until(
                        EC.element_to_be_clickable((By.CLASS_NAME, name ))
                    )
                if mod == 1 or mod == 3:
                    return "True"
                elif mod == 0 or mod == 2:
                    return element
            elif typ == "X":
                if mod == 0 or mod == 1:
                    element = WebDriverWait(self.d, wait_time).until(
                        EC.presence_of_element_located((By.XPATH, name ))
                    )
                elif mod == 2:
                    element = WebDriverWait(self.d, wait_time).until(
                        EC.visibility_of_all_elements_located((By.XPATH, name ))
                    )
                elif mod == 3:
                    element = WebDriverWait(self.d, wait_time).until(
                        EC.element_to_be_clickable((By.XPATH, name ))
                    )
                if mod == 1 or mod == 3:
                    return "True"
                elif mod == 0 or mod == 2:
                    return element
        except:
            return "False"
            self.logger.debug(self.msid + "error: " + name)
    
    @retry(retry_on_result=retry_ifno_element, stop_max_attempt_number=20)
    def cont_cli(self, typ, name):
        self.error = 0
        try:
            if typ == "N":
                cont_ele = self.d.find_element_by_name(name)
            elif typ == "I":
                cont_ele = self.d.find_element_by_id(name)
            elif typ == "C":
                cont_ele = self.d.find_element_by_css_selector(name)
            elif typ == "CN":
                cont_ele = self.d.find_element_by_class_name(name)
            elif typ == "X":
                cont_ele = self.d.find_element_by_xpath(name)

            ActionChains(self.d).context_click(cont_ele).perform()
        except:
            self.error = 1
            e = sys.exc_info()
            self.logger.error(self.msid + str(e))
            self.paparazzi_style(name, mod = 1)
            
    
    @retry(retry_on_result=retry_ifno_element, stop_max_attempt_number=5)
    def doubl_cli(self, typ, name, **kwargs):
        for key, value in kwargs.items():
            locals()[key] = value
        self.error = 0
        try:
            if typ == "N":
                cont_ele = self.d.find_element_by_name(name)
            elif typ == "I":
                cont_ele = self.d.find_element_by_id(name)
            elif typ == "C":
                cont_ele = self.d.find_element_by_css_selector(name)
            elif typ == "CN":
                cont_ele = self.d.find_element_by_class_name(name)
            elif typ == "X":
                cont_ele = self.d.find_element_by_xpath(name)
            elif typ == "M":
                ActionChains(self.d).double_click(name).perform()
                
            if typ != "M":    
                ActionChains(self.d).double_click(cont_ele).perform()
        except:
            self.error = 1

            if 'mod' in kwargs:
                if locals()['mod'] == 0:
                    e = sys.exc_info()
                    self.logger.error(self.msid + str(e))
                    pass
                else:
                    e = sys.exc_info()
                    self.logger.error(self.msid + str(e))
                    self.paparazzi_style(name, mod = locals()['mod'])
            else:        
                e = sys.exc_info()
                self.logger.error(self.msid + str(e))
                self.take_pic(self.hostname)
                if '^' in self.act:
                    typ = ' NB '
                else:
                    typ = 'H'
                bob = reporter(self.msid, typ, smtp_from = self.conf.smtp_from, smtp_pass = self.conf.smtp_pass, rcpt_to = self.conf.rcpt_to, smtp_server = self.conf.smtp_server, smtp_port = self.conf.smtp_port)
                bob.SendMail(self.hostname)
                self.logger.debug(self.msid + "failed id: " + name)
                self.logger.debug(self.msid + "incident has been reported")
                if 'X' in self.act or 'r' in self.act:
                    time.sleep(1)
                    logger.debug(self.msid + "terminating connection")
                    try:
                        self.quiBro()
                    except:
                        pass
                    return

    #switch to frame
    def switch_frame(self, frid, **kwargs):
        locals()['mod'] = 0
        for key, value in kwargs.items():
            locals()[key] = value
        try:
            if frid == "fr-iframe" or locals()['mod'] == 1:
                print('new')
                self.d.switch_to_frame(self.d.find_element_by_class_name(frid))
            else:
                print('old')
                self.d.switch_to_frame(self.d.find_element_by_id(frid))
                
        except:
            e = sys.exc_info()
            self.logger.error(self.msid + str(e))
            self.paparazzi_style(frid)        

    def switch_def(self):
        self.d.switch_to_default_content()

    def redrive(self):
        return self.d

    def retutime(self):
        return self.tp