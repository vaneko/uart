from retrying import retry
import uuid
from autoTest import *
import  multiprocessing
import collections


class testExecutor():
    def __init__(self, host, actions, users, lang, **kwargs):
        self.ini_url = None
        self.session_id = None
        self.usr_pass = None
        self.host = host
        self.act = actions
        self.users = users
        self.lang = lang
        self.id = ' [' + str(uuid.uuid4())[:8] +'] '
        for key, value in kwargs.items():
            setattr(self, key, value)
            

        

    def test_execute(self):
        self.test = autoTest(self.host, self.act, self.users, lang = self.lang, msid = self.id, ini_url = self.ini_url, session_id = self.session_id) #AmYgectnGs
        self.test.start_exec()
        self.my_list = self.test.drive_det
        self.usr_pass = self.test.a_pass
        if '!' in self.act:
            if self.lang == 'All':
                self.list = self.test.list_localise
                self.users = self.test.list_prtcps
                self.langs = self.test.languages
            else:
                self.users = self.test.list_prtcps
                self.langs = self.test.languages
                if self.usr_pass == "pass_set":
                    self.pwds = self.test.user_pass_list
                else:
                    self.user_pass = 'pass'
            return

        elif 'Y' in self.act or 'j' in self.act:
            if self.usr_pass == 'pass':
                upwd = 'I!c@e2@3w@ar!p'
            else:
                upwd = self.usr_pass
            if self.lang == 'All':
                self.act += "Q"
                for usr, lng in self.test.list_localise.items():
                    lstatt = []
                    for i in self.test.list_localise.keys():
                        if usr != i:
                            lstatt.append(i)    
                    sid = ' [' + str(uuid.uuid4())[:8] +'] '
                    prm = usr + ":" + upwd + ";" + ",".join(lstatt)
                    self.test.hostname = self.host
                    if 'W' in self.act:
                        self.hst = self.host + '/svn'
                    else:
                        self.hst = self.host
                    self.test.host = self.hst + '/webmail/'
                    self.test.act = self.act.replace("Y","j") #.replace("j","")
                    self.test.u_name = usr
                    self.test.u_pass = upwd
                    self.test.list_prtcps = lstatt
                    self.test.msid = sid
                    self.test.lang = lng.encode('utf-8')

                    if 's' in self.test.act:
                        self.test.prot = 'https://'
                    else:
                        self.test.prot = 'http://'

                    if 'O' in self.act:
                        self.test.ini_url = self.test.drive_det[0] 
                        self.test.session_id = self.test.drive_det[1]
                    else:
                        self.test.start_exec()
                   


                print("elvis has left the building!")
            else:
                
                
                
                lstatt = {}
                for i in self.test.list_prtcps:
                    lstatt[i] = [x for x in self.test.list_prtcps if x != i]
                if 'U' in self.act:
                    lstatt = collections.OrderedDict(sorted(lstatt.items())) 
                    last = max(lstatt)
                for i in lstatt:
                    sid = ' [' + str(uuid.uuid4())[:8] +'] '
                    self.test.u_name = i
                    if self.usr_pass == 'pass_set':
                        self.test.u_pass = self.test.user_pass_list[i.split('@')[0]]
                    else:
                        print('pwd: ' + str(self.usr_pass))
                        self.test.u_pass = upwd    
                    if 'U' in self.act:
                        if i != last:    
                            print 'last: ' + last
                            prm = self.test.u_name + ":" + self.test.u_pass + ";" + last
                            self.test.list_prtcps = last
                            #self.test.list_prtcps.append(last)
                        else:
                            self.act = self.act.replace(self.act[self.act.find('U'):][:3], 'U0')
                            print self.act
                            temp_list = ""
                            for i in lstatt:
                                if i != max(lstatt):
                                    temp_list += str(lstatt[i]) + ','
                            print temp_list
                            prm = self.test.u_name + ":" + self.test.u_pass + ";" + ",".join(temp_list)
                            self.test.list_prtcps = temp_list
                            
                    else:
                        prm = self.test.u_name + ":" + self.test.u_pass + ";" + ",".join(lstatt)
                        self.test.list_prtcps = lstatt

                    self.act += "Q"
                    self.test.hostname = self.host
                    if 'W' in self.act:
                        self.hst = self.host + '/svn'
                    else:
                        self.hst = self.host
                    self.test.host = self.hst + '/webmail/'

                    self.test.act = self.act.replace("Y","j") #.replace("j","")
                    
                    self.test.msid = sid
                    self.test.lang = self.lang.encode('utf-8')
                    if 's' in self.act:
                        self.test.prot = 'https://'
                    else:
                        self.test.prot = 'http://'
                    if 'O' in self.act:
                        self.test.ini_url = self.test.drive_det[0] 
                        self.test.session_id = self.test.drive_det[1]
                    self.test.start_exec()