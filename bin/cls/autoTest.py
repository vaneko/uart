from radoBot import *
from logger import *
from webAdmin import *
from setupDriver import *
from settSetter import *

class autoTest(radoBot):
    def __init__(self, host,  act, param, **kwargs):
        self.str_time = time.time()
        self.time_to_sleep = 0.1
        self.lang = 'English'
        self.report = {}
        self.env = {}
        self.atc_tim = []
        self.kwarglist = {}
        self.fail = 0
        self.a_pass = 'pass'

        self.conf = settSetter() 
        if os.path.isfile('settings.conf') and os.path.getsize('settings.conf') > 0:
            self.conf.load_settings()
        else:
            self.conf.set_settings()
            self.conf.load_settings() 

        for key, value in kwargs.items():
            setattr(self, key, value)
           
        self.hostname = host
        self.act = act
        if 'W' in self.act:
            self.hst = host + '/svn'
        else:
            self.hst = host
        if 'Y' in self.act or 'y' in self.act:
            self.host = self.hst + '/admin/'
        else:
            self.host = self.hst + '/webmail/'

        self.param = param
        if 's' in act:
            self.prot = 'https://'
        else:
            self.prot = 'http://'
        print self.param
        self.master, self.slave = self.param.split(';')
        self.u_name, self.user_pass = self.master.split(':')
        if self.user_pass == 'pass':
            self.user_pass = self.conf.USER_PWD
        else:
            self.u_pass = self.user_pass
        if ':' in self.slave:
            self.a_pass = 'pass_set'
            self.user_pass_list = {}
        else:
            self.a_pass = self.conf.USER_PWD

        if '*' not in self.slave:
            if ':' in self.slave:
                self.list_prtcps = []
                for i in self.slave.split(','): 
                    name, pw = i.split(':')
                    self.list_prtcps.append(name)
                    self.user_pass_list[name] = pw
                    self.a_pass = 'pass_set'
            else:
                self.list_prtcps = self.slave.split(',')
                self.a_pass = self.conf.USER_PWD

        elif '*' in self.slave:
            self.list_prtcps = []
            u_count, u_mstr = self.slave.split('*')
            if ':' in u_mstr:
                u_mstr, self.a_pass = u_mstr.split(':')
            if '@' in u_mstr:
                u_list = u_mstr.split('@')
                for i in range(int(u_count)):
                    self.list_prtcps.append(u_list[0] + str(i) + '@' + u_list[1])
            else:
                for i in range(int(u_count)):
                    self.list_prtcps.append(u_mstr + str(i))
        


    def add_hostname(self):
        if any('@' not in i for i in self.list_prtcps):
            if type(self.list_prtcps) is list:
                for i in self.list_prtcps:
                    if '@' not in i:
                        print i
                        self.list_prtcps[self.list_prtcps.index(i)] = i + '@' + self.hostname
                    print self.list_prtcps
                    
            else:
                if '@' not in self.list_prtcps:
                    print self.list_prtcps
                    self.list_prtcps = self.list_prtcps + '@' + self.hostname
        


    def start_exec(self):
        if 'O' in self.act:
            
            if 'F' not in self.act:
                self.re_use(self.ini_url, self.session_id, msid = self.msid, lang = self.lang, hostname = self.hostname)
        elif 'Q' in self.act:
            pass
        else:
            self.setBro(self.act, self.msid)
        
        if 'Y' in self.act or 'y' in self.act:
            self.wad_start(self.prot + self.host, self.u_name, self.user_pass)

            if self.lang == 'All':
                self.list_localise = {}
                self.list_prtcps = []

                for i, v  in enumerate(self.languages):
                    if '@' in self.slave:
                        u_list = self.slave.split('@')
                        usrnam = u_list[0] + str(i) + '@' + u_list[1]
                        self.list_prtcps.append(usrnam)
                        self.list_localise[usrnam] = v 
                    else:
                        usrnam = self.slave + str(i)
                        self.list_prtcps.append(usrnam)
                        self.list_localise[usrnam] = v
                        
            self.add_hostname()
            
            self.creat_user()
            last_part = self.list_prtcps[-1]

            for i in self.list_prtcps:
                if len(self.list_prtcps) > 1:
                    if i == last_part:
                        save = 'save'
                    else:
                        save = 'save_another'
                else:
                    save = 'save'
                ii, dom = i.split('@')
                time.sleep( self.tp.retime(0))
                self.ecce_homo(ii, self.a_pass, save)
                time.sleep( self.tp.retime(1))
            
            
            if 'q' in self.act:
                self.wad_logout()
                if 'y' in self.act:
                    self.quiBro()
                    return
                else:
                    return
        else:
            self.add_hostname()
            if 'l' in self.act:
                self.str_time = time.time()
                self.wc_start(self.prot + self.host, self.u_name, self.u_pass)
                if '^' in self.act:
                    self.report['LOGIN'] = self.tim

            if 'j' in self.act:
       
                if self.lang == 'All':
                    self.list_localise = {}
                    self.list_prtcps = []
       
                    for i, v  in enumerate(self.languages):
                        if '@' in self.slave:
                            u_list = self.slave.split('@')
                            usrnam = u_list[0] + str(i) + '@' + u_list[1]
                            self.list_prtcps.append(usrnam)
                            self.list_localise[usrnam] = v 
       
                        else:
                            usrnam = self.slave + str(i)
                            self.list_prtcps.append(usrnam)
                            self.list_localise[usrnam] = v
                    self.logout()
                    
                    return
        if 'N' in self.act:
            dice = [ 'text', 'html']
            mode = random.choice(dice)
            if mode == 'text' and 'n' in self.act:
                self.act = self.act.replace("N", "n")
            self.report['FORMAT'] = mode

        if 'U' in self.act:
            smod = self.act[self.act.find('U'):][:3] 
            self.use_case(smod, partip = self.list_prtcps)
            print(self.shared)
        
        if 'D' in self.act:
            self.admin_disab_notif()

        if 'H' in self.act:
            attend = self.list_prtcps
        else:
            attend = None    
        self.get_new()
        
        #if 'b' in self.act:
        #    self.check_first(0)
        #    self.snd_sms()

        if '#' in self.act:
            self.check_first(0)
            self.user_details()
       
        if 'd' in self.act:
            for i in range(2):
                self.check_first(0)
                time.sleep( self.tp.retime(0))
                self.left_ban("F",  self.tp.retime(4))
                self.create_doc(text = self.lng)
                if self.base.btnerr == 0:
                    self.happy_ending("DOCUMENT")
                    self.report['DOCUMENT'] = self.tim
                    time.sleep( self.tp.retime(2))
                    self.base.press_x("doc", mod = 1)
                    time.sleep( self.tp.retime(2))
                else:
                    self.report['DOCUMENT'] = 0.0
                    
        if 'm' in self.act:
            for i in range(2):
                self.check_first(0)
                self.msg_new()
                self.msg_comp(list(self.list_prtcps))
                self.happy_ending("MESSAGE")
                self.report['MESSAGE'] = self.tim
                self.get_new()
                if 'H' in self.act:
                    self.report['MSG RECIP'] = len(list(self.list_prtcps))
                
        
        
        if 'e' in self.act:
            for i in range(2):
                self.check_first(0)
                self.cal_cli()
                self.creat_eve(1, prtcps = list(self.list_prtcps))
                self.happy_ending("EVENT")
                self.report['EVENT'] = self.tim
                if 'H' in self.act:
                    self.report['EVN ATTEND'] = len(list(self.list_prtcps))
            
                self.get_new()
        
        if 'c' in self.act:
            for i in range(2):
                self.check_first(0)
                self.cnt_new()
                my_p = word_pick(mod=2)
                self.creat_cont(my_p)
                self.happy_ending("CONTACT")
                self.report['CONTACT'] = self.tim
                self.get_new()

        if 't' in self.act:
            for i in range(2):
                self.check_first(0)
                self.task_cli()
                self.creat_task(prtcps = list(self.list_prtcps))
                self.happy_ending("TASK")
                self.report['TASK'] = self.tim
                if 'H' in self.act:
                    self.report['TASK ATTEND'] = len(list(self.list_prtcps))
                self.get_new()
        
        if 'n' in self.act:
            for i in range(2):
                self.check_first(0)
                self.note_cli()
                self.creat_not()
                self.happy_ending("NOTE")
                self.report['NOTE'] = self.tim
                self.get_new()

        if 'i' in self.act:
            self.check_first(0)
            
            self.im_post(prtcps = list(self.list_prtcps))
            if 'H' in self.act:
                self.report['IM RECIP'] = len(list(self.list_prtcps))
       
        if 'v' in self.act:
            self.check_first(0)
            self.video_call()
       
        if 'V' in self.act:
            self.check_first(0)
            self.voip_call()
        
        
        if 'g' in self.act:
            self.check_first(0)
            try:
                self.expand_fpanel()
                tcmod = 'ok'
            except:
                tcmod = 'wait'
            for i in range(2):
                self.team_po(list(self.list_prtcps), tcmod)
                #self.add_tc_room(1)
                for i in range(5):
                    self.team_po(list(self.list_prtcps), tcmod = 'ok')

                self.get_new()

        if 'q' in self.act:
            self.check_first(0)
            time.sleep( self.tp.retime(1))
            self.logout()
       
        end_time = time.time() - self.str_time
        exe_time = timedelta(seconds=end_time)

        self.env['VERSION '] = self.vers
        self.env['USER'] = self.uname
        self.env['BROWSER'] = self.brva
        self.env['LANGUAGE'] = str(self.lng)

        if '^' in self.act:
            typ = ' NB '
        else:
            typ = 'H'
        bob = reporter(self.msid, typ, smtp_from = self.conf.smtp_from, smtp_pass = self.conf.smtp_pass, rcpt_to = self.conf.rcpt_to, smtp_server = self.conf.smtp_server, smtp_port = self.conf.smtp_port)
       
        if 'A' in self.act:
            mx = max(self.atc_tim, key=float)
            mn = min(self.atc_tim, key=float)
            mxt = timedelta(seconds=float(mx))
            mnt = timedelta(seconds=float(mn))
            
            at = 0
            for i in self.atc_tim:
                at += float(i)
            avr = float(at)/len(self.atc_tim)    
            avrg = timedelta(seconds=float(avr))
        else:
            mxt = None
            mnt = None
            avrg = None


        bob.send_report(self.hostname, self.env, self.report, test_duration = exe_time, attach_max_time = mxt, attach_min_time = mnt, attach_avrg_time = avrg)
        #if '~' in self.act:
         #   sys.exit(0)
