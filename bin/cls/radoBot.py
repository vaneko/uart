#from waitTimer import *
import dircache
from compButtons import *
from phpChecker import *
from fileGiver import *
from getProfilepic import *
from imChecker import *
from locatEle import *
from logger import *
from phpChecker import *
from word_pick import *
import subprocess

class radoBot(locatEle):
    """webclient basic operations"""
    
    def tester_pack(self, **kwargs):
        if len(self.kwarglist) < 1: 
            self.kwarglist = { 'logger' : self.logger.get_logger(), 'msid' : self.msid, 'act' : self.act, 'hostname' : self.hostname, 'vers' : self.vers, 'tp' : waitTimer(), 'sleep_timer' : self.sleep_timer, 'time_to_sleep' : self.time_to_sleep, 'lng' : self.lng, 'atc_tim' : self.atc_tim }
        for key,value in kwargs.items():
            self.kwarglist[key] = value
        return self.kwarglist
    
    def frm_gw_handler(self):
        if float(str(self.vers)[:2]) == 12 and float(str(self.vers)[3:][:3]) > 1.1 and 'w' not in self.act:
            return 'gw'
        else:
            return 'frm_file'

    def sub_ob(self, ob, **kwargs):
        for key,value in kwargs.items():
            setattr(ob, key, value)
        self.logger.debug(self.msid + "substitution completed")
        

    
    @retry(stop_max_attempt_number=10)            
    def tim_zon(self):
        self.base.press_ok("timezone")
        self.logger.debug(self.msid + "time zone pop up window")
    
    def login_cred(self, uname, upass):
        print "login"
        self.uname = str(uname)
        self.upass = str(upass)
        self.snd_key("N", "email-address", self.uname.decode('utf-8'), 3 * self.tp.retime(2), clear='yes')
        self.snd_cli("N", "next", 3 * self.tp.retime(2))
        #time.sleep(3)
        #self.snd_cli("CN", ".o-form__element--password.o-form__element", 5 * self.tp.retime(1))
        self.snd_key("N", "password", self.upass.decode('utf-8'), 3 * self.tp.retime(2), clear='yes')
        self.snd_cli("N", "next", 3 * self.tp.retime(2))
            
    
    def diff_account(self):
        self.snd_cli("X", '//*[@id="content"]/main/form/div/div/section[5]/p/a',  self.tp.retime(2))
        self.sleep_timer(self.time_to_sleep)
        self.snd_cli("X", '//*[@id="content"]/main/form/div/div/section[5]/a',  self.tp.retime(2))
    
    def bad_ico(self, method, id):
        if 'W' in self.act or float(str(self.vers)[:4]) >= 12.1 :
            self.snd_cli(method, id,  self.tp.retime(2))
            self.sleep_timer(self.time_to_sleep)
            return 0
        else:
            return 1

    def left_ban(self, typ, tim):
        iid="gui.frm_main.filter#" + typ
        self.snd_cli("I", iid, int(tim))
        self.sleep_timer(self.time_to_sleep)
        if typ != "F" or typ != "M":
            self.cont_cli("I", iid)
    
    @retry(stop_max_attempt_number = 2)
    def expand_fpanel(self):
        #to be redone
        #"noarrow ico img lpreview nodes end"
        self.snd_cli("I", "gui.frm_main.filter#M", self.tp.retime(1))
        self.snd_cli("I", "gui.frm_main.hmenu3/8", 2 * self.tp.retime(2), mod = 1)
        print "click 1"
        self.snd_cli("I", "gui.frm_main.hmenu3/8/4", 5 * self.tp.retime(1), mod = 0)
        print "click 2"
        self.snd_cli("I", "gui.frm_main.hmenu3/8/4", 5 * self.tp.retime(1), mod = 0)
        print "click 3"
        self.snd_cli("I", "gui.frm_main.hmenu3/8/4/0", 5 * self.tp.retime(1), mod = 0)
        print "click 4"
        self.logger.debug(self.msid + "expand folder panel")


    def sleep_timer(self, val):
        if val == 0:
            pass
        else:
            time.sleep(val)
        
    def day_picker(self, start = None):
        if start == None:
            start = 1
        day = random.randint(int(start),31)
        print day
        if day != int(31):
            did = 'gui.calendar_block.calendar/' + str(day)
            self.snd_cli("I", did, 5 * self.tp.retime(1), mod = 1)
            self.logger.debug(self.msid + "picked " + str(day))

        else:
            try:
                self.snd_cli("I", "gui.calendar_block.calendar.right1", 5 * self.tp.retime(1))
                day = random.randint(1,31)
                did = 'gui.calendar_block.calendar/' + str(day)
                self.snd_cli("I", did, 5 * self.tp.retime(1), mod = 1)
                self.logger.debug(self.msid + "picked " + str(day))
            except:
                day2 = 30
                did = 'gui.calendar_block.calendar/' + str(day2)
                self.snd_cli("I", did, 5 * self.tp.retime(1), mod = 1)
                self.logger.debug(self.msid + "picked " + str(day2))
        return int(day)




    def cal_ico_cli(self, strn, t_mod):
        self.logger.error("TO DOOOOOOO !!!!!")
        if t_mod == 1 or t_mod == 4:
            if strn == 'gw':
                self.snd_cli("I", "gui." + strn + ".startDate.input",  self.tp.retime(3), mod = 2)
                if t_mod == 4:
                    dstart = self.get_ele("C", '.today.active', 'text', 5 * self.tp.retime(1))
                    start = self.day_picker(start = dstart)
            else:
                print strn
                self.snd_cli("I", "gui." + strn + ".startDate.input",  self.tp.retime(3))
                start = self.day_picker()
            if 'R' not in self.act:
                end = random.randint(int(start), 31)
                if strn == 'gw':
                    if self.vers >= 12.1:
                        self.snd_cli("I", "gui." + strn + ".endDate.input",  self.tp.retime(3), mod = 2)
                    else:                    
                        self.snd_cli("I", "gui." + strn + ".tab1.endDate.input",  self.tp.retime(3), mod = 2)
                    if t_mod == 4:
                        self.day_picker(end)
                else:
                    if self.vers >= 12.1:
                        self.snd_cli("I", "gui." + strn + ".endDate.input",  self.tp.retime(3))
                    else:
                        self.snd_cli("I", "gui." + strn + ".maintab.tab1.endDate.input",  self.tp.retime(3))

                    self.day_picker(end)
        elif t_mod == 2:
            self.logger.error(self.msid + "create jira ticket")
            if strn == 'gw':
                self.snd_cli("I", "gui." + strn + ".maintab.tab1.EVNENDDATE.input",  self.tp.retime(3), mod = 2)
            else:
                self.snd_cli("I", "gui." + strn + ".maintab.tab1.EVNENDDATE.input",  self.tp.retime(3))
            start = self.day_picker()
            if 'R' not in self.act:
                end = random.randint(int(start), 31)
                if strn == 'gw':
                    self.snd_cli("I", "gui." + strn + ".maintab.tab1.EVNSTARTDATE.input",  self.tp.retime(3), mod = 2)
                else:
                    self.snd_cli("I", "gui." + strn + ".maintab.tab1.EVNSTARTDATE.input",  self.tp.retime(3))
                    self.day_picker(end)
        elif t_mod == 3:
            if strn == 'gw':
                self.snd_cli("I", "gui." + strn + ".maintab.tab2.ITMANNIVERSARY.input",  self.tp.retime(3), mod = 2)
            else:
                self.snd_cli("I", "gui." + strn + ".maintab.tab2.ITMANNIVERSARY.input",  self.tp.retime(3))
                bday = self.day_picker()
        else:
            if strn == 'gw':
                self.snd_cli("I", "gui." + strn + ".maintab.tab2.ITMBDATE.input",  self.tp.retime(3), mod = 2)
            else:
                self.snd_cli("I", "gui." + strn + ".maintab.tab2.ITMBDATE.input",  self.tp.retime(3))
                bday = self.day_picker()

    def change_year(self, year):
        self.snd_key("I", "gui.calendar_block.calendar.year#main", year, 5 * self.tp.retime(1), clear = "yes", retr = 1, mod = 0 )
        
    def whats_new(self, start_time):
        evn_start = start_time
        self.tim = str(time.time() - evn_start)
        self.sleep_timer(self.time_to_sleep)
        self.snd_cli("I", "gui.whatsnew.btn_next#main", 5 * self.tp.retime(1), mod = 1)
        self.logger.debug(self.msid + " login time: " + self.tim)
        self.sleep_timer(self.time_to_sleep)
        self.snd_cli("I", "gui.whatsnew.btn_next#main", 1, mod = 1)
        self.sleep_timer(self.time_to_sleep)
        self.snd_cli("I", "gui.whatsnew.btn_next#main", 1, mod = 1 )
        self.sleep_timer(self.time_to_sleep)
        self.snd_cli("I", "gui.whatsnew.btn_next#main", 1, mod = 1)
        self.sleep_timer(self.time_to_sleep)



    def check_first(self, mod):
        if mod == 1:
            sleeper = 60
        elif mod == 2:
            sleeper = 10
        else:
            sleeper = 2
        evn_startTime = time.time()
        if 'W' in self.act or float(str(self.vers)[:4]) >= 12.1 :
            if len(str(self.return_multi_elem("I", "gui.whatsnew#container", "CN", "o-well.o-well--error.o-header__well.is-visible.atoms-well", sleeper, mod = 1))) > 1:
                
                if self.ret_ele("I", "gui.whatsnew#container", 1, self.tp.retime(0)) == "True":
                    self.whats_new(evn_startTime)
                else:
                    self.tim = str(time.time() - evn_startTime)
                    self.paparazzi_style("LOGIN_FAILED")
                    self.tim = str(time.time() - evn_startTime)
        else:
            self.tim = str(time.time() - evn_startTime)
            

    def options_menu(self):
        if 'W' in self.act or float(str(self.vers)[:4]) >= 12.1:
            self.snd_cli('I', "gui.frm_main.stat/5", 2 * self.tp.retime(4))
        else:
            self.snd_cli('I', "gui.frm_main.stat/3", 2 * self.tp.retime(4))
    
    

    def xss_compose(self, containr, **kwargs):
        locals()['mod'] = 'D'
        for key,value in kwargs.items():
            locals()[key]=value

        if locals()['mod'] == 'M':
            self.base.msg_options(containr)
            self.snd_cli("I", "gui." + containr + ".mode_select", self.tp.retime(3))
            self.snd_cli("X", "//*[@rel='code']", self.tp.retime(1))
            self.base.msg_options(containr)
        else:
            self.bad_ico("C", ".ico.toggle_format_toolbar")
            self.snd_cli("I", "gui." + containr + ".select", 3 * self.tp.retime(1))
            self.snd_cli("X", "//*[@rel='code']", self.tp.retime(1))
        self.snd_key("X", "//textarea[@dir='auto']", word_pick(mod = 3).gime_som(), self.tp.retime(3), clear = "yes")
        print(word_pick(mod = 3).gime_som())
        


    
    def check_help(self):
        self.logger.debug(self.msid + "user help check")
        self.options_menu()
        self.sleep_timer(self.time_to_sleep)
        self.snd_cli("CN", "ico.help", 1 * self.tp.retime(2), mod = 1)
        self.sleep_timer(self.time_to_sleep)
        if self.error == 0:
            if 'g' in self.act:
                self.snd_cli("I", "gui.help.maintab/chat", 5 * self.tp.retime(1), mod=1)
                self.sleep_timer(self.time_to_sleep)
            self.snd_cli("I", "gui.help.maintab/apps", 5 * self.tp.retime(1), mod=1)
            self.sleep_timer(self.time_to_sleep)
            self.snd_cli("I", "gui.help.maintab/about", 5 * self.tp.retime(1), mod=1)
            self.sleep_timer(self.time_to_sleep)
            try:
                self.base.press_x("help", mod = 0)
            except:
                self.snd_cli("CN", "close",  self.tp.retime(3), mod = 0)
                self.base.press_x("help", mod = 1)

            self.sleep_timer(self.time_to_sleep)
            self.logger.debug(self.msid + "user help check finished")
        else:
            self.logger.error(self.msid + " user help check skiped due script error")


    def wc_start(self, url, uname, upass):
        self.url = url
        self.uname = uname
        self.upass = upass
        self.login(self.url)
        self.vers = self.get_ele('X', '//*[@id="content"]/div/section[3]/p', 'title',  self.tp.retime(4))
        self.logger.debug(self.msid + self.vers)            
        if self.lang != None and self.lang != 'None':
            self.sleep_timer(self.tp.retime(0))
            self.fetch_lang(self.lang)
            self.logger.debug(self.msid + "language(s) fetched")
            self.sleep_timer(self.tp.retime(0))
        else:
            self.lng = 'None'
        if 'O' in self.act or 'Q' in self.act:
            self.sleep_timer(self.tp.retime(0))
            self.diff_account()
            self.sleep_timer(self.tp.retime(0))
        kwargs = self.tester_pack()
        self.login_cred(self.uname, self.upass)
        evn_startTime = time.time()
        if 'j' not in self.act and 'D' not in self.act and '!' not in self.act:
            if self.ret_ele("I", "gui.timezone.x_btn_ok", 1, 5 * self.tp.retime(1)) == "True":
                self.tim_zon()
            if self.ret_ele("I", "gui.whatsnew#rem", 1,  2 * self.tp.retime(1)) == "True":
                try:
                    self.snd_cli("I", "gui.whatsnew#rem", 5 * self.tp.retime(1), mod = 0)
                except WebDriverException as e:
                    if 'Element is not clickable at point' in e.msg:
                        self.snd_cli("CN", "close", self.tp.retime(1), mod = 0)
                        self.snd_cli("I", "gui.whatsnew#rem", 5 * self.tp.retime(1), mod = 1)
        self.logger.debug(self.msid + "loading " + self.uname + " " + self.act)
        
        self.base = compButtons(self.redrive())
        self.base.conf = self.conf
        self.sub_ob(self.base, **kwargs)
        self.tester_pack(base = self.base)
        
        if '^' in self.act:
            if 'W' in self.act or float(str(self.vers)[:4]) >= 12.1:
                self.check_first(1)
            else:   
                try:
                    self.snd_cli("CN", "close",  self.tp.retime(2), mod = 0)
                    self.snd_cli("I", "gui.whatsnew#rem",  self.tp.retime(3), mod = 0 )
                except WebDriverException as e:
                    if 'Element is not clickable at point' in e.msg:
                        self.snd_cli("CN", "close",  self.tp.retime(2), mod = 0)
                        self.snd_cli("I", "gui.whatsnew#rem",  self.tp.retime(2), mod = 1)
                self.tim = str(time.time() - evn_startTime)
                self.logger.debug(self.msid + " login time: " + self.tim)
        else:
            self.check_first(2)
        if 'A' in self.act:
            self.attach = fileGiver(self.redrive())
            self.sub_ob(self.attach, **self.kwarglist)
            self.attach.conf = self.conf
        if '^' in self.act:
            self.check_help()


    def wad_start(self, url, uname, upass, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)  
            
        self.url = url
        self.uname = uname
        self.upass = upass
        
        try:
            self.login(self.url)
            self.vers = self.get_ele('X', '//*[@id="content"]/div/section[3]/p', 'title',  self.tp.retime(2))
            self.logger.debug(self.msid + self.vers)
            if self.lang != None and self.lang != 'None':
                print self.lang
                self.fetch_lang(self.lang)
                self.logger.debug(self.msid + "language(s) fetched")
            else:
                self.lng = 'None'
            self.logger.debug(self.msid + "entering username")
            self.sleep_timer( self.tp.retime(0))
            self.snd_key("N", "email-address", self.uname, 5 * self.tp.retime(1))
            self.snd_cli("N", "next", 5 * self.tp.retime(1))
            self.logger.debug(self.msid + "entering password")
            self.snd_key("N", "password", self.upass, 5 * self.tp.retime(1), clear='yes')
            self.snd_cli("N", "next", 5 * self.tp.retime(1))
            self.logger.debug(self.msid + "login proceed")
        except:
            pass
            

    def set_api(self, param):
        "webadmin only"
        self.logger.debug(self.msid + "setting api: " + param)
        self.snd_cli("I", "gui.frm_main.btn_menu_main#main", 5 * self.tp.retime(1))
        self.snd_cli("X", "//*[starts-with(@id, 'gui.frm_main.aside_main_menu#tabmenu-api_console')]", 5 * self.tp.retime(1))
        ap_var, ap_val = param.split('::')
        self.snd_key("I", "gui.popup.main.input_search#main", ap_var + Keys.RETURN, 5 * self.tp.retime(1))
        self.logger.debug(self.msid + 'searched for API: ' + ap_var)
        val_ele = str('//*[@id="gui.popup.main.consoledialog.list.obj_' + ap_var + '#main"]')
        self.snd_key("X", val_ele, ap_val, 5 * self.tp.retime(1), clear='yes')
        self.logger.debug(self.msid + 'set api ' + ap_var + ':' + ap_val)
        self.sav_ele = str('gui.popup.main.consoledialog.list.obj_' + ap_var + '_save#main')
        self.snd_cli("I", sav_ele, 5 * self.tp.retime(1))
        self.logger.debug(self.msid + "api saved")
        self.snd_cli("I", "gui.popup.main.btn_close_search#main", 5 * self.tp.retime(1))
        self.logger.debug(self.msid + 'api ended')




    def creat_user(self):
        "webadmin only"
        self.logger.debug(self.msid + "user mode")
        self.sleep_timer(self.time_to_sleep)
        self.snd_cli("I", "gui.frm_main.btn_menu_add#main", self.tp.retime(4))
        self.logger.debug(self.msid + "adding new user")
        self.sleep_timer(self.time_to_sleep)
        self.snd_cli("I", "gui.frm_main.aside_menu#tabmenu-new_user",  self.tp.retime(2))
    
    
    def ecce_homo(self, uname, upass, save):
        "webadmin only"
        self.logger.debug(self.msid + "creating user")
        sur_name = word_pick(text = self.lng, txt_mod = self.act).gime_som()
        self.sleep_timer(self.time_to_sleep)
        print sur_name
        
        self.snd_key("I", "gui.popup.main.newaccount.input_name#main", uname,  self.tp.retime(3), clear="yes")
        self.sleep_timer(self.time_to_sleep)
        self.snd_key("I", "gui.popup.main.newaccount.input_surname#main", sur_name, self.tp.retime(2), clear="yes")
        self.sleep_timer(self.time_to_sleep)
        self.snd_key("I", "gui.popup.main.newaccount.input_alias#main", uname, self.tp.retime(2), clear="yes")
        self.sleep_timer(self.time_to_sleep)
        self.snd_key("I", "gui.popup.main.newaccount.input_password#main", upass, self.tp.retime(2), clear='yes')
        #self.paparazzi_style('attachment', mod = 1)  
        self.sleep_timer(self.time_to_sleep)
        self.snd_cli("I", "gui.popup.main.btn_" + save + "#main",  self.tp.retime(2))
        self.logger.debug(self.msid + "created user: " + uname + " " + sur_name.decode('utf-8'))
        self.sleep_timer(5)

    def admin_disab_notif(self):
        self.snd_cli('I', "gui.frm_main.stat/3", 5 * self.tp.retime(1))
        self.snd_cli('I', "gui.frm_main.stat/3/7", 5 * self.tp.retime(1))
        self.snd_cli('I', "gui.settings.maintab/general_settings", 5 * self.tp.retime(1))
        self.snd_cli('I', "gui.settings.maintab.general_settings.maintab.layout.notifications#main", 5 * self.tp.retime(1))
        self.snd_cli('X', "/html/body/div[1]/div[9]/div/a[3]", 5 * self.tp.retime(1))
        self.snd_cli('I', "gui.settings.maintab.general_settings.maintab.layout.x_notifications_set.domadmin", 5 * self.tp.retime(1))
        self.base.press_ok("settings")
        self.sleep_timer(self.time_to_sleep)
        self.quiBro()

    def item_tag(self, string):
        if 'x' in self.act:
            l_mod = 4
        else:
            l_mod = 1
        self.snd_key("I", "gui." + string + ".input.plus#main", word_pick(text=self.lng, mod = l_mod).gime_som(), 5 * self.tp.retime(1), retr = 1)
        self.logger.debug(self.msid + string + "tagged")
        time.sleep(5)


    def user_details(self):
        self.options_menu()
        self.snd_cli("C", '.info', 5 * self.tp.retime(1), mod = 0)
        if self.error == 1:
            self.options_menu()
            self.snd_cli("C", '.info', 5 * self.tp.retime(1), mod = 1)
        if self.error == 0:
            time.sleep( self.tp.retime(0))
            self.snd_key("I", "gui.gw.maintab.tab1.X_NAME#main", word_pick(text=(self.uname + ' ' + self.lng)).gime_som(), self.tp.retime(1), clear = "yes")
            if 'x' in self.act:
                self.snd_key("I", "gui.gw.maintab.tab1.ITMCLASSIFYAS#main", "<img src=e onerror=alert(/show-as/)>", self.tp.retime(3), clear= "yes")
           
            user = getProfilepic()
            if '$' in self.act:
                mod = 0
            else:
                mod = 1
            user.get_pic(mod)
            self.pic = os.path.abspath(user.pic)
            #self.sex = user.sex
            time.sleep(2 * self.tp.retime(1))
            self.send_file("gui.gw.maintab.tab1.X_AVATAR.file#file", self.pic)
            self.sleep_timer(self.time_to_sleep)
            if self.ret_ele("I", "gui.gw.x_btn_ok#main", 3,2 * self.tp.retime(4)) == True:
                pass
            else:
                self.logger.error(self.msid + "ERROR loading profile picture")

            os.remove(os.path.abspath(self.pic))
            if 'T' in self.act:
                self.item_tag("gw.maintab.tab1.ITMCATEGORY")
                
            #personal
            self.snd_cli("I", "gui.gw.maintab/tab2", self.tp.retime(3))
            #bday
            year = random.randint(1896, 2018)
            self.cal_ico_cli('gw', 0)
            self.change_year(year)
            bday = self.day_picker()
            self.sleep_timer(self.time_to_sleep)
            #aniv
            year = random.randint(1896, 2018)
            self.cal_ico_cli('gw', 3)
            self.change_year(year)
            bday = self.day_picker()


            self.base.press_ok('gw')
            self.sleep_timer(self.time_to_sleep)
        else:
            self.logger.error(self.msid + "user details skipped due script error")
        
    def user_options(self):
        self.user_details()
        self.snd_cli("C", "ico.settings", 5 * self.tp.retime(1))

    def options_tab(self):
        #Mail
        self.snd_cli("I", "gui.settings.maintab/mail_settings", 5 * self.tp.retime(1))
        #sing
        self.snd_cli("I", "gui.settings.maintab.mail_settings.maintab/signature", 5 * self.tp.retime(1))

        self.base.press_ok('gw')



    
    def happy_ending(self, gwev, strn = None):
        self.logger.debug(self.msid + "start processing " + gwev)
        evn_startTime = time.time()
        if gwev == "DOCUMENT":
            selector = "div.notification.folder_subscribed"
        elif gwev == "MESSAGE":
            selector = "div.notification.message_sent"
            strn = "frm_compose"
        elif gwev == "CONTACT":
            selector = "div.notification.invitation_sent"
            strn = "frm_contact"
        else:
            selector = "div.notification.item_saved"
        
        clss = selector.split(".")
        clstr = clss[1] + " " + clss[2]
            
        try:
            notifs = self.return_multi_elem("C", selector, "C", "div.notification.alert", 6 * self.tp.retime(4))
            self.notif = notifs[0]
        except:
            self.notif = 'E'
            
            
            
        if len(str(self.notif)) > 1:
            notif_type = self.notif.get_attribute("class")
            if notif_type == "notification alert":
                self.logger.debug(self.msid + "except")
                self.logger.error(self.msid + "not: " + str(self.ret_ele("C", "div.header", 0, 5 * self.tp.retime(1)).text.encode('UTF-8')))
                self.tim = str(time.time() - evn_startTime)
                self.take_pic(self.hostname)
                if '^' in self.act:
                    typ = ' NB '
                else:
                    typ = ' H '
                if float(str(self.vers)[:4]) >= 12.1 and str(self.vers)[:8] != '12.1.1.0':
                    self.snd_cli("CN", "close",  self.tp.retime(3))
                bob = reporter(self.msid, typ, self.hostname, self.env, self.report, test_duration = exe_tim, attach_max_time = mxt, attach_min_time = mnt, attach_avrg_time = avrg)
                bob.SendMail(self.hostname)
                self.logger.debug(self.msid + gwev + " proc time: " + self.tim)
                if strn != None:
                    if gwev == "MESSAGE":
                        self.doubl_cli("I", "gui.frm_compose#title", mod = 0)
                        self.base.press_x(strn, mod = 0)
                    else:
                        try:
                            self.snd_cli("C", "div.notification.alert", self.tp.retime(1), mod = 0)
                            self.base.press_x(strn, mod = 0)
                        except:
                            try:
                                self.snd_cli("C", "div.notification.alert", self.tp.retime(1), mod = 0)
                                self.snd_cli("C", ".close",  self.tp.retime(3), mod = 0)
                                self.base.press_x(strn, mod = 1)
                            except:
                                self.snd_cli("C", "div.notification.alert", self.tp.retime(1), mod = 0)
                                self.snd_cli("CN", "close",  self.tp.retime(3), mod = 0)
                                self.base.press_x(strn, mod = 1)
                     
                    self.logger.debug(self.msid + gwev + " closing dialog")
                    if gwev == "MESSAGE":
                        self.base.press_ok("frm_confirm")
                        self.logger.debug(self.msid + gwev + " saving to drafts")
                
            elif notif_type == clstr: 
                #msg = self.ret_ele("C", selector, 0, 2 * self.tp.retime(1)).text
                try:
                    self.logger.debug(self.msid + "NOT: " + str(self.ret_ele("C", selector, 0, 2 * self.tp.retime(1)).text.encode('UTF-8')))
                except:
                    if 'x' in self.act:
                        self.logger.error(self.msid + "!!! XSS vulnerability in " + gwev + " discovered !!!")
                        self.logger.error(self.msid + "!!! XSS vulnerability in " + gwev + " discovered !!!")
                        self.logger.error(self.msid + "!!! XSS vulnerability in " + gwev + " discovered !!!")
                        self.logger.error(self.msid + "!!! XSS vulnerability in " + gwev + " discovered !!!")
                        self.paparazzi_style(gwev, mod = 3)

                self.tim = str(time.time() - evn_startTime)
                
                if float(str(self.vers)[:4]) >= 12.1 and str(self.vers)[:8] != '12.1.1.0':
                    self.snd_cli("C", selector, self.tp.retime(1))
                    if gwev == "MESSAGE":
                            self.snd_cli("C", "div.button_label", self.tp.retime(1), mod = 1)
                            if self.error == 0:
                                try:
                                    self.base.refresh('frm_delivery')
                                    self.sleep_timer(2*self.time_to_sleep)
                                    self.base.press_cancel('frm_delivery')
                                except:
                                    self.paparazzi_style('details', mod = 1)
                            else:
                                self.logger.debug(self.msid + "delivery report skipped due undefined error")

                    else:
                        self.snd_cli("C", ".close", 5 * self.tp.retime(1), mod = 0)
                    
                self.logger.info(' ' + self.msid + "225 " + gwev +" success")
                self.logger.debug(self.msid + gwev + " proc time: " + self.tim)

                
        else:
            self.tim = str(time.time() - evn_startTime)
            self.logger.debug(self.msid + gwev + " proc time-out: " + self.tim)
            a = phpChecker(self.redrive())
            a.checkphp(self.tp.retime(1))
            self.paparazzi_style(a.phpstat, mod = a.pap)
            
            
            #self.paparazzi_style(gwev, mod = 2)
    
   
    
    def get_new(self):
        if 'f' in self.act:
            self.snd_cli("I", "gui.frm_main.filter#M", 3 * self.tp.retime(2))
            self.snd_cli("I", "gui.frm_main.hmenu1/0", 3 * self.tp.retime(2))
            self.logger.debug(self.msid + "got new")
            if self.ret_ele("I", 'gui.whatsnew#rem', 1, self.tp.retime(1)) == 'True':
                self.snd_cli("I", "gui.whatsnew#rem", 5 * self.tp.retime(1))
                self.logger.debug(self.msid + "whats new closed")



    def msg_dropfile(self):
        msg_drop="gui.frm_compose.attach_control.file.file#file"
        for i in range(5):
            if os.path.isdir('docs'):
                fdir = os.path.abspath('docs')
                file = random.choice(dircache.listdir(os.path.abspath(fdir)))
                path = os.path.join(fdir, file)
                self.send_file( msg_drop, path)
    

    def msg_new(self):
        self.left_ban("M", self.tp.retime(4))
        self.sleep_timer(self.time_to_sleep)
        self.logger.debug(self.msid + "left menu inbox")

    
    
    
    def msg_comp(self, prtcps, msg_mod = 0):
        self.sleep_timer(self.time_to_sleep)
        self.prtcps = prtcps
        for i in self.prtcps:
            self.snd_key("I", "gui.frm_compose.to.plus#main", i + ';', 5 * self.tp.retime(1),)
            self.logger.debug(self.msid + "msg rcptto: " + i)
        
        time.sleep(1)
        if 'W' in self.act or float(str(self.vers)[:4]) >= 12.1 :
            
            self.snd_key("I", "gui.frm_compose.to.plus#main", " ", 5 * self.tp.retime(1), retr = 1)
            if 'g' in self.act:
                self.snd_cli("I", "gui.frm_compose#switch", 5 * self.tp.retime(1))
                self.snd_cli("I", "gui.frm_compose.lbl_chat#main", 2 * self.tp.retime(2))
                self.snd_key("I", "gui.frm_select_folder.tree_folder.inp_search#main", "teamchat", 5 * self.tp.retime(1), retr = 1)
                self.snd_cli("C", ".none.end.active",  self.tp.retime(2), mod = 0)
                self.base.press_ok("frm_select_folder")
                self.snd_key("I", "gui.frm_compose.teamchat_message#main", word_pick(text ='Check out ' + self.lng, txt_mod = self.act).gime_som(), 5 * self.tp.retime(1))

        self.snd_key("I", "gui.frm_compose.subject#main", word_pick(text='Big chance of ' + self.lng, txt_mod = self.act).gime_som(), 5 * self.tp.retime(1))
        if 'x' not in self.act:
            if 'W' in self.act or float(str(self.vers)[:4]) >= 12.1: 
                self.switch_frame("fr-iframe")
            else:
                self.switch_frame("gui.frm_compose.body#frame")
        
            self.snd_key("X", "/html/body", word_pick(mod=1, text="Dear Sir/Madam\n", txt_mod = self.act).gime_som(), 5 * self.tp.retime(1))
            self.snd_key("X", "/html/body", word_pick(text="\nI would like to ask you for", txt_mod = self.act).gime_som(), 5 * self.tp.retime(1))
            self.snd_key("X", "/html/body", word_pick(text="\nIt's really urgent due to", txt_mod = self.act).gime_som(), 5 * self.tp.retime(1))
            self.snd_key("X", "/html/body", word_pick(text="\notherwise we all could be", txt_mod = self.act).gime_som(), 5 * self.tp.retime(1))
            self.snd_key("X", "/html/body", word_pick(text="\n\nyours sincerely" + self.lng, txt_mod = self.act).gime_som(), 5 * self.tp.retime(1))
        
            self.switch_def()
        
            self.sleep_timer(self.time_to_sleep)
            if 'A' in self.act and self.attach.fail < 2:
                if ( 'W' in self.act or float(str(self.vers)[:4]) >= 12.1 ) and (msg_mod == 0 or msg_mod == 3):
                    if str(self.vers)[:8] != '12.1.1.0' :
                        self.snd_cli("I", "gui.frm_compose.x_btn_send#main",  self.tp.retime(2))
                        try:
                            notifs = self.return_multi_elem("C", "div.notification.send_message", "C", "div.notification.alert", 6 * self.tp.retime(4))
                            self.notif = notifs[0]
                        except:
                            self.notif = 'E'
                        
                        if len(str(self.notif)) > 1:
                            notif_type = self.notif.get_attribute("class")
                        else:
                            notif_type = "BAD"
                        if notif_type == "notification alert":
                            err = str(self.ret_ele("C", "div.header", 0, 5 * self.tp.retime(1)).text.encode('UTF-8'))
                            self.logger.error(self.msid + "ERROR: " + err )
                            self.paparazzi_style(err, mod = 1)
                            self.error = 1                            
                        elif self.ret_ele("C", "div.notification.send_message", 1, self.tp.retime(4) ) == "True":
                            self.logger.debug(self.msid + "Damn I forgott the attachment!")
                            self.snd_cli("C", "div.notification.send_message", self.tp.retime(1))
                            self.logger.info(self.msid + "NOT: " + str(self.ret_ele("C", "div.notification.send_message", 0, self.tp.retime(1)).text.encode('UTF-8')))
                            if self.error == 0:
                                self.snd_cli("C", "div.button_label", self.tp.retime(4))
                                if float(str(self.vers)[:2]) == 12 and float(str(self.vers)[3:][:3]) > 1.1 and 'w' not in self.act:
                                    #sry but that is really not nice from devel, WC-7839
                                    # not at all nice, it's really complicated
                                    if msg_mod == 3:
                                        self.snd_cli("X", '/html/body/div[1]/div[6]/div/div[2]/div/div/div[3]/table/tbody/tr/th[2]/form/div', 2, self.tp.retime(2))
                                            
                                    else:
                                        parent = self.ret_ele("X", '//*[@class="arrow"]', 2, self.tp.retime(2))[2]
                                        parent.click()
                                    self.logger.debug(self.msid + "WC-7839 workaround")
                                    self.snd_cli("I", "gui.cmenu/1", self.tp.retime(3), mod = 1)

                                else:
                                    self.snd_cli("C", ".ico.always.icogw", self.tp.retime(3))
                            else:
                                self.logger.error(self.msid + "attachment skipped due undefined script error")
                        else:
                            self.logger.error(self.msid + "undexpected error")
                            self.paparazzi_style('UNKNOWN ERROR', mod = 2)

                        
                        
                            
                    else:
                        self.snd_cli("C", ".ico.always.icogw", self.tp.retime(3))
                        
                else:
                    if msg_mod == 0:
                        self.snd_cli("I", "gui.frm_compose.attach_control.item#main",  self.tp.retime(2))
                if self.error == 0 and ( msg_mod == 0 or msg_mod == 3):
                    self.attach.attachment()
            elif 'A' in self.act and self.attach.fail >=  2:
                self.logger.error(self.msid + "attachment skipped due " + str(self.attach.fail) + " errors" )
        else:
            self.xss_compose('frm_compose', mod = 'M')

        self.snd_cli("I", "gui.frm_compose.x_btn_send#main",  self.tp.retime(2))
        self.logger.debug(self.msid + "msg sent")

    
    def add_tc_room(self, stat):
        self.snd_cli("I", "gui.frm_main.bar.tree.folders.btn_add", self.tp.retime(2), mod = 1)
        if self.error == 0:
            self.logger.debug(self.msid + "adding teamchat room")
            self.snd_key("I", "gui.frm_add_room.input_name#main", word_pick(text=self.lng, txt_mod=self.act).gime_som(), 5* self.tp.retime(1))
            el_id = "gui.frm_add_room.select_type" + str(stat)
            self.snd_cli("I", el_id, 5*self.tp.retime(1), mod = 1)
            self.base.press_ok("frm_add_room")
        if str(stat) == str(1):
            self.snd_cli("C", ".obj_button.big.ico.img.add.simple", 3 * self.tp.retime(3))
            self.snd_cli("I", "gui.cmenu/0", 5*self.tp.retime(1), mod = 1)
            self.snd_cli("I", "gui.invite.btn_to", 5*self.tp.retime(1), mod = 1)
            self.add_atten(self.prtcps, el_id = 'add_address')
            self.base.press_ok('add_address')
            self.snd_key("I", "gui.invite.note#main", word_pick(text=self.lng, txt_mod=self.act).gime_som(), 5*self.tp.retime(1))
            self.snd_cli("I", "gui.invite.btn_invite#main", self.tp.retime(1), mod = 1)
            self.logger.debug(self.msid + "members invited")
            

            

    def team_po(self, users, tcmod):
        time.sleep(0.1)
        self.snd_cli("I", "gui.frm_main.filter#I", 3 * self.tp.retime(3))
        if tcmod != 'ok':
            self.snd_cli("I", 'gui.frm_main.bar.main#pin', 5 * self.tp.retime(1))
            self.logger.debug(self.msid + "folder panel pinned")
        self.logger.debug(self.msid + "teamchat operations")
        time.sleep(3)
        scrollbar = self.ret_ele("I", "gui.frm_main.main.tabs.room.list#refresh", 0,  self.tp.retime(2))
        if len(scrollbar.text) > 0:
            self.snd_cli("I", "gui.frm_main.main.tabs.room.list#refresh", 5 * self.tp.retime(1))
            self.logger.debug(self.msid + "tc scrolled down")
            time.sleep(0.1)       
        
        self.snd_key("I", "gui.frm_main.main.text.input#main", word_pick(text="Lets speak about", txt_mod = self.act).gime_som(), 5 * self.tp.retime(1), retr = 1)
        self.snd_key("I", "gui.frm_main.main.text.input#main", word_pick(text="the secret of " + self.lng, txt_mod = self.act).gime_som(), 5 * self.tp.retime(1), retr = 1)
        self.snd_key("I", "gui.frm_main.main.text.input#main", word_pick(text="what makes us more", txt_mod = self.act).gime_som(), 5 * self.tp.retime(1), retr = 1)
        time.sleep(1)
        
        

        if "d" in self.act or 'A' in self.act:
            self.logger.debug(self.msid + "tc doc started")
            self.create_doc(text = self.lng)
            if self.base.btnerr == 0:
                self.happy_ending("TC-DOC")
                self.report['TC DOCUMENT'] = self.tim
                time.sleep(3)
                self.base.press_x("doc", mod = 1)
                    
                self.logger.debug(self.msid + "tc doc finished")
                time.sleep(5)
            else:
                self.report['TC DOCUMENT'] = 0.0
                
                
        
        if "A" in self.act and self.attach.fail <= 0:
            self.logger.debug(self.msid + "tc file started")

            self.snd_cli("C", ".obj_button.big.ico.img.add.simple", 3 * self.tp.retime(3))
            self.snd_cli("C", ".ico2.attach", 5 * self.tp.retime(1))
            usr = random.choice(users)
            print users
            print usr
            self.attach.attachment(mod=1, user=usr)
            self.logger.debug(self.msid + "tc file finished")
        #perf fatal error
        if "e" in self.act:
            self.logger.debug(self.msid + "tc event started")
            self.snd_cli("C", ".obj_button.big.ico.img.add.simple", 3 * self.tp.retime(3))
            self.snd_cli("C", ".ico2.event", 5 * self.tp.retime(1))
            self.creat_eve(2)
            self.happy_ending("EVENT")
            self.report['TC EVENT'] = self.tim
            self.logger.debug(self.msid + "tc event finished")
            time.sleep(1)
        
        if 'W' in self.act or float(str(self.vers)[:4]) >= 12.1: 
        
            if 'm' in self.act:
                self.snd_cli("C", ".obj_button.big.ico.img.add.simple", 3 * self.tp.retime(3))
                self.snd_cli("C", ".ico2.email.end", 5 * self.tp.retime(1))
                if float(str(self.vers)[:2]) == 12 and float(str(self.vers)[3:][:3]) < 1.2 or 'w' in self.act:
                    mmd = 0
                elif float(str(self.vers)[:2]) >= 12 and float(str(self.vers)[3:][:3]) >= 1.2:
                    mmd = 3
                else:
                    mmd = 1
                print mmd
                self.msg_comp(users, msg_mod = mmd)
                self.happy_ending("MESSAGE")
            
        if 'p' not in self.act and self.notif != 'E':
            if "checked" in self.get_ele("I", "gui.frm_main.main.tabs.notify", "class", 2 * self.tp.retime(2)):
                self.sleep_timer(3)
                try:
                    self.snd_cli("I", "gui.frm_main.main.tabs.notify", 3 * self.tp.retime(2), mod = 0)
                except:
                    pass


        self.sleep_timer(self.time_to_sleep)

    def cnt_new(self):
        self.left_ban("C", self.tp.retime(4))
    
    
    def create_doc(self, **kwargs):
        for key, value in kwargs.items():
            keywords = {key:value}
            mstr = word_pick(**keywords).gime_som()
        
        base_str = str(self.frm_gw_handler())
        self.logger.debug(self.msid + "document")
        self.sleep_timer(self.time_to_sleep)
        if float(str(self.vers)[:4]) >= 12.1 and str(self.vers)[:8] != '12.1.1.1':  
            self.js_hoover()
        else:
            self.cont_cli("C", ".ico.ico_new.nodes.end")
        xid = ['.item.word', '.item.excel', '.item.powerpoint']
        choic = random.choice(xid)
        try:
            self.snd_cli("C", choic,  self.tp.retime(2), mod = 0)
        except:
            self.get_new()
            self.cont_cli("C", ".ico.ico_new.nodes.end")
            self.snd_cli("C", choic,  self.tp.retime(3), mod = 0 )
            
            
        self.snd_key("I", "gui."+base_str+"#name", str(mstr).lstrip(),  self.tp.retime(3))
        self.base.press_ok(base_str, mod = 1)
        if self.base.btnerr == 1:
            if 'A' in self.act:
                self.attach.fail = 2
                self.logger.error(self.msid + "Document ERROR, possibly WC-7829")
        time.sleep(3)



    def creat_cont(self, cntx):
        self.val = cntx
        if float(str(self.vers)[:2]) == 12 and float(str(self.vers)[3:][:3]) < 1.2 or 'w' in self.act:
            containr = "frm_contact"
        else:
            containr = "gw"
        self.snd_key("I", "gui." + containr + ".maintab.tab1.X_NAME#main", self.val.gime_som(), 3 * self.tp.retime(2))
        self.snd_key("I", "gui." + containr + ".maintab.tab1.ITMNICKNAME#main", self.val.gime_som(), 5 * self.tp.retime(1))
        self.snd_key("I", "gui." + containr + ".maintab.tab1.ITMCOMPANY#main", self.val.gime_som(), 5 * self.tp.retime(1))
        self.snd_key("I", "gui." + containr + ".maintab.tab1.ITMJOBTITLE#main", self.val.gime_som(), 5 * self.tp.retime(1))
        self.snd_key("I", "gui." + containr + ".maintab.tab1.X_PHONE1.PHNNUMBER.inp#main", self.val.gime_som(), 5 * self.tp.retime(1) )
        self.snd_key("I", "gui." + containr + ".maintab.tab1.X_EMAIL1.inp#main", self.val.gime_som(), 5 * self.tp.retime(1))
        self.logger.debug(self.msid + "contact general finished")

        #personal
        self.snd_cli("I", "gui." + containr + ".maintab/tab2", 5 * self.tp.retime(1))
        self.cal_ico_cli(containr,  3 * self.tp.retime(1))
        self.snd_cli("I", "gui." + containr + ".maintab.tab2.ITMGENDER",  self.tp.retime(1))
        time.sleep(0.1)
        if '12.0' in self.vers:
            gender = ['//*[@id="gui.optionlist"]/div/a[1]', '//*[@id="gui.optionlist"]/div/a[2]']
        else:
            gender = ['/html/body/div[1]/div[9]/div/a[2]', '/html/body/div[1]/div[9]/div/a[1]']
        try:
            sheorhe = random.choice(gender)
            self.snd_cli("X", sheorhe,  self.tp.retime(2), mod=1)
        except:
            self.logger.debug(self.msid + "well, its good to know that")
        self.logger.debug(self.msid + "contact personal finished")
        
        #busines
        self.snd_cli("I", "gui." + containr + ".maintab/tab3", 5 * self.tp.retime(1))
        self.snd_key("I", "gui." + containr + ".maintab.tab3.ITMPROFESSION#main", self.val.gime_som(), 5 * self.tp.retime(1))
        self.snd_key("I", "gui." + containr + ".maintab.tab3.ITMASSISTANTNAME#main", self.val.gime_som(), 5 * self.tp.retime(1))
        self.snd_key("I", "gui." + containr + ".maintab.tab3.X_WEB.inp#main", "www." + self.val.gime_som() + ".com", 5 * self.tp.retime(1))
        self.logger.debug(self.msid + "contact business tab finished")

        #note
        self.snd_cli("I", "gui." + containr + ".maintab/tab4", 5 * self.tp.retime(1))
        if 'N' in self.act:
            if self.bad_ico("C", ".ico.toggle_format_toolbar") > 0:
                self.snd_cli("I", "gui." + containr + ".maintab.tab4.ITMDESCRIPTION.select",  self.tp.retime(2))
                self.snd_cli("X", '//*[@id="gui.optionlist"]/div/a[1]',  self.tp.retime(2))
            self.snd_cli("I", "gui." + containr + ".maintab.tab4.ITMDESCRIPTION/bold",  self.tp.retime(2))
        if 'W' in self.act or float(str(self.vers)[:4]) >= 12.1: 
            self.switch_frame("fr-iframe")
        else:
            self.switch_frame("gui." + containr + ".maintab.tab4.ITMDESCRIPTION#frame")

        self.snd_key("X", "/html/body", "My notes about this " + self.val.gime_som(), 5 * self.tp.retime(1))
        self.snd_key("X", "/html/body", "\nthis person is total " + self.val.gime_som(), 5 * self.tp.retime(1))
        self.snd_key("X", "/html/body", "\nbetter do not start talking about " + self.val.gime_som(), 5 * self.tp.retime(1))
        self.snd_key("X", "/html/body", "\notherwise you will know what does it mean " + self.val.gime_som(), 5 * self.tp.retime(1))
        self.switch_def()

        #attach
        if 'A' in self.act and self.attach.fail < 2:
            self.snd_cli("I", "gui." + containr + ".maintab/tab5", 5 * self.tp.retime(1))
            self.snd_cli("I", "gui." + containr + ".maintab.tab5.X_ATTACHMENTS.add_item#main", 5 * self.tp.retime(1))
            self.attach.attachment()
        elif 'A' in self.act and self.attach.fail >=  2:
            self.logger.error(self.msid + "attachment skipped due " + str(self.attach.fail) + " errors" )

        #ok
        self.base.press_ok(containr)

    
    
    
    
    
    def task_cli(self):
        self.left_ban("T", 3 * self.tp.retime(2))
        self.logger.debug(self.msid + "task icon right-click")

    
    
    
    
    def add_atten(self, particips, el_id = 'address_book'):
        ulist = []
        if ',' in particips:
            ulist = particips.split(',')
        elif type(particips) == list:
            ulist = particips
        else:
            ulist.append(particips)
        time.sleep(0.1)
        #inp = self.ret_ele("I", "gui.address_book.search.search#main", 0,  self.tp.retime(2))
        for i in ulist:
            if i != self.uname:
                self.snd_key("I", "gui."+el_id+".search.search#main" , "email:" + str(i),  self.tp.retime(2), clear=1, retr=1)
                self.logger.debug(self.msid + "adding attten " + i)
                time.sleep(1)
                self.doubl_cli("X", "//*[starts-with(@id, 'gui."+el_id+".grid') and contains(@style, 'top: 0px; height: 33px;')]", mod = 1)
                self.logger.debug(self.msid + "send return")
                time.sleep(1)
            
            
    def set_rights(self, r_mod = 'rnd'):
        if float(str(self.vers)[:2]) >= 12 and float(str(self.vers)[3:][:3]) > 1.1:
            rights = {0:'read', 1:'author', 2:'write', 3:'all', 4:'full'}
        else:    
            rights = {0:'read', 1:'write', 2:'all', 3:'full'}

        if r_mod == 'rnd':
            rkey = random.choice(rights.keys())
            self.u_right = rights[rkey] 
        else:
            rkey = [ k for k, v in rights.iteritems() if v == r_mod ][0]
            self.u_right = r_mod
        
        self.snd_cli("I", "gui.cmenu/" + str(rkey), 3* self.tp.retime(2))

    def subscribe_button(self):
        subj = self.get_ele("I", "gui.frm_main.main.mailview#subject", 'text', self.tp.retime(0))
        self.switch_frame("gui.frm_main.main.mailview#frame")
        if self.ret_ele("CN", "obj_button.color1", 1, self.tp.retime(1)) == "True":
            action = self.get_ele("CN", "obj_button.color1", 'value', self.tp.retime(0) )
            self.logger.debug(self.msid + str(subj).encode('UTF-8'))
            self.snd_cli("CN", "obj_button.color1",self.tp.retime(0), mod =1 )
            if self.error == 0:
                self.logger.debug(self.msid + str(action).encode('UTF-8'))
        else:
            print 'NO'
        self.switch_def()
    
    def check_msg(self):
        msgs = self.ret_ele("X", "//*[starts-with(@id, 'gui.frm_main.main.list') and contains(@style, 'height: 25px;')]",2, self.tp.retime(2) )
        for i in msgs:
            try:
                i.click()
                time.sleep(1)
            except:
                i.click()
                time.sleep(1)
            self.subscribe_button()
            

    def use_case(self, smod, partip = 'admin', rmod = 'rnd' ):
        print smod
        if smod == 'U11':
            #share account
            self.cont_cli("I", "gui.frm_main.bar.main#title")
            if self.error == 0:
                self.snd_cli("I", "gui.cmenu/4", 3 * self.tp.retime(1), mod = 1)
        elif smod == 'U12':
            #share inbox
            self.cont_cli("CN", "ico.ico_inbox")
            if self.error == 0:
                self.snd_cli("I", "gui.cmenu/9", 3 * self.tp.retime(1), mod = 1)
            
        if 'U0' not in smod and smod != 'U13':
            self.shared = {}        
            if ',' in partip:
                partip = partip.split(',')
                nm = 0
                for i in partip:
                    self.snd_cli("I", "gui.frm_sharing.add#main", 3*self.tp.retime(1), mod =1)
                    self.add_atten(i)
                    self.base.press_ok('address_book')
                    self.snd_cli("I", "gui.frm_sharing.users/" + str(nm) + "/perm", 3*self.tp.retime(1), mod = 1)
                    nm = nm + 1
                    self.set_rights(r_mod = rmod)
                    self.shared[i] = self.u_right
            else:
                self.snd_cli("I", "gui.frm_sharing.add#main", 3*self.tp.retime(1), mod =1)
                self.add_atten(partip)
                self.base.press_ok('address_book')
                self.snd_cli("I", "gui.frm_sharing.users/0/perm", 3*self.tp.retime(1), mod = 1)
                self.set_rights(r_mod = rmod)
                self.shared[partip] = self.u_right
            self.base.press_ok('frm_sharing')
        #u13 todo
        else:
            self.check_msg()
    
    def creat_task(self, **kwargs):
        if float(str(self.vers)[:2]) == 12 and float(str(self.vers)[3:][:3]) < 1.2 or 'w' in self.act:
            containr = "frm_task"
        else:
            containr = "gw"
        self.tx = word_pick(text = self.lng, txt_mod = self.act)
        for key, value in kwargs.items():
            setattr(self, key, value)
        self.logger.debug(self.msid +"TASK begun")        

        
        self.cal_ico_cli(containr, 2)
        self.logger.debug(self.msid + "task date set")
        self.snd_key("I", "gui." + containr + ".maintab.tab1.EVNTITLE#main", "Guest of " + self.tx.gime_som(), 5 * self.tp.retime(1))

        #note
        if 'N' in self.act:
            if self.bad_ico("C", ".ico.toggle_format_toolbar") > 0:
                self.snd_cli("I", "gui." + containr + ".maintab.tab1.EVNNOTE.select", 5 * self.tp.retime(1))
                self.snd_cli("X", '//*[@id="gui.optionlist"]/div/a[1]',  self.tp.retime(2))
            self.logger.debug(self.msid + "task set html")
            self.snd_cli("I", "gui." + containr + ".maintab.tab1.EVNNOTE/bold", 5 * self.tp.retime(1))
            self.logger.debug(self.msid + "task set bold")    
        if 'N' in self.act or 'n' in self.act:    
            if 'W' in self.act or float(str(self.vers)[:4]) >= 12.1: 
                self.switch_frame("fr-iframe")
            else:
                self.switch_frame("gui." + containr + ".maintab.tab1.EVNNOTE#frame")
        
            #body
            self.snd_key("X", "/html/body", "Hi\n its required to " + self.tx.gime_som(), 5 * self.tp.retime(1))
            self.snd_key("X", "/html/body", "\nbe aware of breaking " + self.lng + self.tx.gime_som(), 5 * self.tp.retime(1))
            self.snd_key("X", "/html/body", "\nin case of emergency call " + self.lng + self.tx.gime_som(), 5 * self.tp.retime(1))
            self.switch_def()
        if 'N' in self.act:
            if self.bad_ico("C", ".ico.toggle_format_toolbar") > 0:
                pass
        time.sleep( self.tp.retime(0))

        if 'M' in self.act:
            self.snd_cli("I", "gui." + containr + ".maintab.tab1.X_REMINDERS.checkbox_1", 5 * self.tp.retime(1))
           
        if 'T' in self.act:
            self.item_tag("" + containr + ".maintab.tab1.EVNTYPE")
            #self.snd_key("I", "gui..input.plus#main", self.lng, 5 * self.tp.retime(1), retr = 1)
            #self.logger.debug(self.msid + "task tagged")

        self.logger.debug(self.msid + "task general finished")

        #repeating
        if 'R' in self.act:
            self.snd_cli("I", "gui." + containr + ".maintab/tab2", 5 * self.tp.retime(1))
            self.logger.debug(self.msid + "task repeating tab")
            self.snd_cli("I", "gui." + containr + ".maintab.tab2.X_REPEATING.radio_repeats4", 5 * self.tp.retime(1))
            self.snd_cli("I", "gui." + containr + ".maintab.tab2.X_REPEATING.radio_ends2", 5 * self.tp.retime(1))
            self.snd_key("I", "gui." + containr + ".maintab.tab2.X_REPEATING.RCRCOUNT#main", "3", 5 * self.tp.retime(1), clear='yes')
            self.logger.debug(self.msid + "repeating yearly - count 3")
        
        #participants
        if 'H' in self.act:
            self.snd_cli("I", "gui." + containr + ".maintab/tab5", 5 * self.tp.retime(1))
            self.snd_cli("I", "gui." + containr + ".maintab.tab5.X_ATTENDEES.add_button#main", 5 * self.tp.retime(1))
            #attendee=""
            #for i in self.prtcps:
             #   attendee += i +','
            #self.sleep_timer(0.1)
            #self.snd_key("I", "gui.address_book.input_0#main", attendee, 5 * self.tp.retime(1))
            self.add_atten(self.prtcps)
            self.sleep_timer(self.time_to_sleep)
            self.logger.debug(self.msid + "task attendee added ")
            if self.ret_ele("I", "gui.address_book.x_btn_ok#main", 3, self.tp.retime(1)) == "True":
                self.base.press_ok('address_book')
                self.logger.debug(self.msid + "task participants finished")
            else:
                self.base.press_x("address-book")
            self.sleep_timer(self.time_to_sleep)

        if 'A' in self.act and self.attach.fail < 2:
            self.logger.debug(self.msid + "task add attachment")
            self.snd_cli("I", "gui." + containr + ".maintab/tab4", 5 * self.tp.retime(1))
            self.snd_cli("I", "gui." + containr + ".maintab.tab4.X_ATTACHMENTS.add_item#main", 5 * self.tp.retime(1))
            self.attach.attachment()
        elif 'A' in self.act and self.attach.fail >=  2:
            self.logger.error(self.msid + "attachment skipped due " + str(self.attach.fail) + " errors" )


        #ok
        self.base.press_ok(containr)

    
    
    
    
    
    def note_cli(self):
        self.left_ban("N",  self.tp.retime(3))
        self.logger.debug(self.msid + "note right-click")

    
    
    
    
    
    
    
    
    def creat_not(self):
        if float(str(self.vers)[:2]) == 12 and float(str(self.vers)[3:][:3]) < 1.2 and 'w' not in self.act:
            containr = "frm_note"
        else:
            containr = "gw"
        self.tx = word_pick(text =self.lng, txt_mod = self.act)
        self.snd_key("I", "gui." + containr + ".maintab.tab1.EVNTITLE#main", self.tx.gime_som() + " note", 3 * self.tp.retime(2))
        
        if 'N' in self.act:
            if self.bad_ico("C", ".ico.toggle_format_toolbar") > 0:
                self.snd_cli("I", "gui." + containr + ".maintab.tab1.NOTE_TEXT.select#main",  self.tp.retime(3))
                self.snd_cli("X", '//*[@id="gui.optionlist"]/div/a[1]',  self.tp.retime(3))
            self.logger.debug(self.msid + "note input set html")
            self.snd_cli("X", '//*[@id="gui.' + containr + '.maintab.tab1.NOTE_TEXT/bold"]', 5 * self.tp.retime(1))
        if 'W' in self.act or float(str(self.vers)[:4]) >= 12.1: 
            self.switch_frame("fr-iframe")
        else:
            self.switch_frame("gui." + containr + ".maintab.tab1.NOTE_TEXT#frame")
        self.snd_key("X", "/html/body", self.tx.gime_som() + " note", 5 * self.tp.retime(1))
        
        if 'N' in self.act or 'n' in self.act:    
            self.switch_def()
            if 'N' in self.act:
                    self.snd_cli("X", '//*[@id="gui.' + containr + '.maintab.tab1.NOTE_TEXT/bold"]', 5 * self.tp.retime(1))
            if 'W' in self.act or float(str(self.vers)[:4]) >= 12.1: 
                self.switch_frame("fr-iframe")
            else:
                self.switch_frame("gui." + containr + ".maintab.tab1.NOTE_TEXT#frame")
            self.snd_key("X", "/html/body", "\nMost important is to " + self.tx.gime_som(), 5 * self.tp.retime(1))
            self.snd_key("X", "/html/body", "\nbecause of " + self.lng +self.tx.gime_som(), 5 * self.tp.retime(1))
            self.snd_key("X", "/html/body", "\nremember this word: " + self.lng + self.tx.gime_som(), 5 * self.tp.retime(1))
            self.switch_def()
            if 'N' in self.act:
                if self.bad_ico("C", ".ico.toggle_format_toolbar") > 0:
                    pass
        
        if 'T' in self.act:
            self.item_tag(containr + ".maintab.tab1.EVNTYPE")
            #self.snd_key("I", "gui..input.plus#main", self.lng, 5 * self.tp.retime(1), retr = 1)
            #self.logger.debug(self.msid + "note tagged")

        if 'A' in self.act and self.attach.fail < 2:
            self.logger.debug(self.msid + "attach started")
            self.snd_cli("I", "gui." + containr + ".maintab/tab2", 5 * self.tp.retime(1))
            self.snd_cli("I", "gui." + containr + ".maintab.tab2.X_ATTACHMENTS.add_item#main", 5 * self.tp.retime(1))
            self.attach.attachment()
        elif 'A' in self.act and self.attach.fail >=  2:
            self.logger.error(self.msid + "attachment skipped due " + str(self.attach.fail) + " errors" )
        #ok
        self.base.press_ok(containr)

    
    
    
    
    
    
    def voip_call(self):
        if 'V' in self.act:
            self.snd_cli("I", "gui.frm_main.stat.btn_sip",  self.tp.retime(3))
            self.snd_key("I", "gui.dial.number#main", 'ondrej.vanek@icewarp.com', 5 * self.tp.retime(1))
            self.snd_cli("I", "gui.dial.call#main", 5 * self.tp.retime(1))
            time.sleep(10)            
            self.snd_cli("X", '//*[@id="gui.sipalert"]/ul/li[2]', 5 * self.tp.retime(1))
            self.base.press_x("dial")    
    
    def video_call(self):
        if 'v' in self.act:
            self.snd_cli("I", "gui.frm_main.stat.btn_sip",  self.tp.retime(3))
            self.snd_key("I", "gui.dial.number#main", 'ondrej.vanek@icewarp.com', 5 * self.tp.retime(1))
            self.snd_cli("I", "gui.dial.video", 5 * self.tp.retime(1))
            time.sleep(10)            
            self.snd_cli("X", '//*[@id="gui.sipalert"]/ul/li[2]', 5 * self.tp.retime(1))
            self.base.press_x("dial")
    
    def cal_cli(self):
        self.left_ban("E", 3 * self.tp.retime(2))

    
    def add_resource(self):
        plus_b = self.ret_ele("X", '//*[@id="gui.frm_event2.x_suggest"]/div', 2, self.tp.retime(2))[0]
        plus_b.click()        
        self.snd_cli("I", "gui.address_book.select#main", self.tp.retime(3), mod = 1)
        self.snd_cli("X", "/html/body/div[1]/div[10]/div/a[4]", self.tp.retime(3), mod = 1)
        if self.ret_ele("X", "//*[starts-with(@d, 'gui.address_book.grid') and contains(@style, 'top: 0px; height: 33px;')]", 3, self.tp.retime(2)) == 'True':
            self.doubl_cli("X", "//*[starts-with(@id, 'gui.address_book.grid') and contains(@style, 'top: 0px; height: 33px;')]", mod = 1)
        if self.error == 0:
            self.base.press_ok('address_book')
            self.logger.debug(self.msid + 'resource added')
        else:
            self.base.press_cancel('address_book')
            self.logger.error(self.msid + 'resource failed')
    
    def creat_eve(self, prm, **kwargs):
        self.tx = word_pick(txt_mod = self.act)        
        for key, value in kwargs.items():
            setattr(self, key, value)
        
        self.prm = prm
        
        self.logger.debug(self.msid + "EVENT begin")
        if float(str(self.vers)[:2]) <= 12 and float(str(self.vers)[3:][:3]) < 1.1 and 'w' not in self.act:
            if int(prm) == 0 or int(prm) == 2:
                containr = "gw"
            elif int(prm) == 1:
                containr = "frm_event"
            
            self.snd_key("I", "gui."+containr+".maintab.tab1.EVNTITLE#main", "party of " + self.tx.gime_som(), 5 * self.tp.retime(1))
            self.cal_ico_cli(str(containr + ".maintab.tab1"), 1)
            #reminder
            if 'M' in self.act and self.prm != 0:
                self.snd_cli("I", "gui."+ containr +".maintab.tab1.X_REMINDERS.checkbox_1", 5 * self.tp.retime(1))
                self.snd_key("I", "gui."+ containr +".maintab.tab1.X_REMINDERS.reminder_1.time.x_text#main", "15", 5 * self.tp.retime(1), clear='yes')
        
            #note
            if 'N' in self.act or 'n' in self.act:
                if 'N' in self.act:
                    if self.bad_ico("C", ".ico.toggle_format_toolbar") > 0:
                        print "bad"           
                        self.snd_cli("X", '//*[@id="gui.' + containr + '.maintab.tab1.EVNNOTE.select#main"]',  self.tp.retime(3))
                        self.snd_cli("X", '//*[@id="gui.optionlist"]/div/a[1]',  self.tp.retime(3))
                    else:
                        print "ok"
                    self.snd_cli("X", '//*[@id="gui.'+ containr +'.maintab.tab1.EVNNOTE/bold"]', 5 * self.tp.retime(1))

                if 'W' in self.act or float(str(self.vers)[:4]) >= 12.1: 
                   self.switch_frame("fr-iframe")
                else:
                    self.switch_frame("gui." + containr + ".maintab.tab1.EVNNOTE#frame")
                self.snd_key("X", "/html/body", "this meeting is about " + self.lng + self.tx.gime_som(), 5 * self.tp.retime(1))
                self.switch_def()
                if 'N' in self.act:
                    self.snd_cli("X", '//*[@id="gui.'+ containr +'.maintab.tab1.EVNNOTE/bold"]', 5 * self.tp.retime(1))
                if 'W' in self.act or float(str(self.vers)[:4]) >= 12.1: 
                    self.switch_frame("fr-iframe")
                else:
                    self.switch_frame("gui." + containr + ".maintab.tab1.EVNNOTE#frame")
                self.snd_key("X", "/html/body", "\n...so prepare " +self.lng +  self.tx.gime_som(), 5 * self.tp.retime(1))
                self.snd_key("X", "/html/body", "\nLet`s " + self.tx.gime_som(), 5 * self.tp.retime(1))
                self.snd_key("X", "/html/body", "\n" + self.lng + " " + self.tx.gime_som(), 5 * self.tp.retime(1))
                self.switch_def()
                if 'N' in self.act:
                    if self.bad_ico("C", ".ico.toggle_format_toolbar") > 0:
                        pass
            #tag
            if 'T' in self.act:
                self.item_tag(containr + ".EVNTYPE")
        
            #reccurence
            if 'R' in self.act:
                self.logger.debug(self.msid + "event repeating tab")
                self.snd_cli("I", "gui."+ containr +".maintab/tab2", 5 * self.tp.retime(1))
                self.snd_cli("I", "gui." + containr + ".maintab.tab2.X_REPEATING.radio_repeats2", 5 * self.tp.retime(1))
                self.logger.debug(self.msid + "event repeat weekly")
                self.snd_cli("I", "gui." + containr + ".maintab.tab2.X_REPEATING.radio_ends2", 5 * self.tp.retime(1))
                self.snd_key("I", "gui." + containr + ".maintab.tab2.X_REPEATING.RCRCOUNT#main", "3", 5 * self.tp.retime(1))
                self.logger.debug(self.msid + "event end count 3")


            #attendee
            if 'H' in self.act and prm != 0:
                if self.prtcps is not None:
                    self.snd_cli("I", "gui." + containr + ".maintab/tab3", 5 * self.tp.retime(1))
                    for i in self.prtcps:
                        self.snd_key("I", "gui." + containr + ".maintab.tab3.X_SCHEDULE.X_TIMETABLE.inp_add#main", i, 5 * self.tp.retime(1), retr = 1)
                        self.logger.debug(self.msid + 'event attendee: ' + i)
                        self.sleep_timer(self.tp.retime(0))
            #attachment
            if 'A' in self.act and self.attach.fail < 2:
                self.snd_cli("I", "gui." + containr + ".maintab/tab5", 5 * self.tp.retime(1))
                self.snd_cli("I", "gui." + containr + ".maintab.tab5.X_ATTACHMENTS.add_item#main", 5 * self.tp.retime(1))
                self.attach.attachment()
            elif 'A' in self.act and self.attach.fail >=  2:
                self.logger.error(self.msid + "attachment skipped due " + str(self.attach.fail) + " errors" )
                #ok
        else:
            if float(str(self.vers)[:2]) == 12 and float(str(self.vers)[3:][:3]) < 1.2 or 'w' in self.act:
                if int(prm) == 0 or int(prm) == 2:
                    containr = "gw"
                elif int(prm) == 1:
                    containr = "frm_event2"
            else:
                    containr = "gw"
                
            self.snd_key("I", "gui."+containr+".EVNTITLE#main", "party of " + self.tx.gime_som(), 5 * self.tp.retime(1))
            if int(prm) == 2:
                self.cal_ico_cli(containr, 4)
            else:
                self.cal_ico_cli(containr, 1)
            if containr == "gw":
                self.snd_cli("I", "gui."+containr+"#collapse_time",  self.tp.retime(2), mod = 2)
            else:
                self.snd_cli("I", "gui."+containr+"#collapse_time",  self.tp.retime(2))
                
            
            time.sleep(0.1)
            if 'A' in self.act and self.attach.fail < 2:
                self.snd_cli("I", "gui." + containr + ".X_ATTACHMENTS.add_item#main", 5 * self.tp.retime(1))
                self.attach.attachment()
            elif 'A' in self.act and self.attach.fail >=  2:
                self.logger.error(self.msid + "attachment skipped due " + str(self.attach.fail) + " errors" )

            if 'H' in self.act and prm != 0:
                    #self.add_resource()
                    if self.prtcps is not None:
                        for i in self.prtcps:
                            self.snd_key("I", "gui."+containr+".x_suggest#main", i, 5 * self.tp.retime(1), retr = 1)

             
             
            if 'N' in self.act or 'n' in self.act:
                if 'W' in self.act or float(str(self.vers)[:4]) >= 12.1 :
                    note = '.x__note'
                else:
                    note = '.note'
                self.snd_cli("I", "gui." + containr + ".x_note#main", 5 * self.tp.retime(1))
                if 'x' not in self.act:
                    if 'N' in self.act:
                        self.bad_ico("C", ".ico.toggle_format_toolbar")
                        self.snd_cli("X", '//*[@id="gui.'+ containr + note + '/bold"]', 5 * self.tp.retime(1))
                    self.switch_frame("fr-iframe")
                    self.snd_key("X", "/html/body", "this meeting is about " + self.lng + self.tx.gime_som(), 5 * self.tp.retime(1))
                    self.switch_def()
                    if 'N' in self.act:
                        self.snd_cli("X", '//*[@id="gui.'+ containr +note + '/bold"]', 5 * self.tp.retime(1))
                    self.switch_frame("fr-iframe")
                    self.snd_key("X", "/html/body", "\n...so prepare " + self.lng + self.tx.gime_som(), 5 * self.tp.retime(1))
                    self.snd_key("X", "/html/body", "\nLet`s " + self.tx.gime_som(), 5 * self.tp.retime(1))
                    self.snd_key("X", "/html/body", "\n" + self.lng + " " + self.tx.gime_som(), 5 * self.tp.retime(1))
                
                    self.switch_def()
                    if 'N' in self.act:
                        self.bad_ico("C", ".ico.toggle_format_toolbar")
                    self.sleep_timer(self.time_to_sleep) 
                    self.snd_cli("C", ".close.done_editing", 5 * self.tp.retime(1))
                else:
                    self.xss_compose(containr + note)

            
            if 'T' in self.act:
                self.item_tag(containr + ".EVNTYPE")
                #self.snd_key("I", "gui." + containr + ".EVNTYPE.input.plus#main", self.lng,  self.tp.retime(2), retr = 1)
            
             

        self.base.press_ok(containr)
        
    def snd_sms(self, number):
        self.number = number
        self.cont_cli("I", 'gui.frm_main.hmenu1/2')
        self.snd_cli("X", '//*[@id="bodyTag"]/div[3]/div[1]/div[3]/div[5]', 5 * self.tp.retime(1))
        self.snd_key("I", "gui.frm_compose.sms.plus#main", self.number, 5 * self.tp.retime(1))
        if self.mod == 'start':
            self.snd_key("I", "gui.frm_compose.body#main", "started test of: " + self.host, 5 * self.tp.retime(1))
        else:
            self.snd_key("I", "gui.frm_compose.body#main", "finished: " + self.host , 5 * self.tp.retime(1))
            
        self.snd_cli("I", "gui.frm_compose.x_btn_send#main", 5 * self.tp.retime(1))
        time.sleep(5)


    def im_actions(self, user):
        self.snd_key("I", "gui.frm_chat.tabs.inp_add#main", user,  self.tp.retime(2), retr = 1, clear = 1)
        root_elem='gui.frm_chat.tabs.tab'
        tail_elem='.text.input#main'
        plus_elem='.text.add'
        drop_tail='.upload.file#file'
        self.sleep_timer(self.time_to_sleep)
        base_elem= str(root_elem) + str(tail_elem)
        
        im_post="Hi " + str(user) + ", can you speak " + str(self.lng) + " ?" 
        
        print "TODO"

        if self.ret_ele("I", base_elem, 3, 0.5) == "True":
            print "yes"
            for s in range(5):
                self.snd_key("I", base_elem, im_post, self.tp.retime(1), retr = 1 )
                self.snd_key("I", base_elem, word_pick(text="what does it mean", txt_mod = self.act).gime_som() + "?", self.tp.retime(1), retr = 1)
                if os.path.isdir('docs'):
                    fdir = os.path.abspath('docs')
                    file = random.choice(dircache.listdir(os.path.abspath(fdir)))
                    path = os.path.join(fdir, file)
                    im_drop = root_elem + drop_tail
                    self.send_file( im_drop, path)
                    self.sleep_timer(self.time_to_sleep)
                    self.sleep_timer(self.time_to_sleep)
            
            if 'A' in self.act and self.attach.fail < 2:
                plus_button = root_elem + plus_elem
                self.snd_cli("I", plus_button, 5 * self.tp.retime(1))
                self.snd_cli("C", ".ico2.attach", 5 * self.tp.retime(1))
                self.attach.attachment()
                self.sleep_timer(self.time_to_sleep)
            elif 'A' in self.act and self.attach.fail >=  2:
                self.logger.error(self.msid + "attachment skipped due " + str(self.attach.fail) + " errors" )
    
                    
        else:
            print "not"
            for i in range(99):
                base_elem2 = root_elem + '_' + str(i) + tail_elem
                if self.ret_ele("I", base_elem2, 3,  self.tp.retime(0)) == "True":
                    for a in range(5):
                    
                        self.snd_key("I", base_elem2, im_post, self.tp.retime(1), retr = 1 )
                        self.snd_key("I", base_elem2, word_pick(text="what does it mean", txt_mod = self.act).gime_som() + "?", 2, retr = 1)
                        self.sleep_timer(self.time_to_sleep)
                        if os.path.isdir('docs'):
                            fdir = os.path.abspath('docs')
                            file = random.choice(dircache.listdir(os.path.abspath(fdir)))
                            path = os.path.join(fdir, file).decode('utf-8')
                            im_drop = root_elem + '_' + str(i) + drop_tail
                            self.send_file( im_drop, path)
                            self.sleep_timer(self.time_to_sleep)
                            self.sleep_timer(self.time_to_sleep)
                    
                    if 'A' in self.act and self.attach.fail < 2:
                        plus_button = root_elem + '_' + str(i) + plus_elem
                        self.snd_cli("I", plus_button, 5 * self.tp.retime(1))
                        self.snd_cli("C", ".ico2.attach", 5 * self.tp.retime(1))
                        self.attach.attachment()
                        self.sleep_timer(self.time_to_sleep)
                        self.sleep_timer(self.time_to_sleep)
                    elif 'A' in self.act and self.attach.fail >=  2:
                        self.logger.error(self.msid + "attachment skipped due " + str(self.attach.fail) + " errors")
        
                            
                    break
            
                    
                   
            

    def im_post(self, prtcps):
        #logger = self.logger.get_logger()
        a = imChecker(self.redrive())
        kwargs = self.tester_pack()
        self.sub_ob(a, **kwargs)
        a.check_im()
        
            
        self.snd_cli("I", "gui.frm_main.filter#M", 5 * self.tp.retime(1))
        if self.ret_ele("I", "gui.frm_main.im#recent", 0, 5 * self.tp.retime(1)) == "True":
            self.snd_cli("I", "gui.frm_main.im#recent", 5 * self.tp.retime(1))
        else:
            self.cont_cli("I", "gui.frm_main.hmenu1/2")
            self.snd_cli("C", ".item.chat", 5 * self.tp.retime(1))
        self.sleep_timer(self.time_to_sleep)
        for i in prtcps:
            if i != self.uname :
                self.im_actions(i)
        self.sleep_timer(self.time_to_sleep)
        try:
            self.base.press_x('frm_chat', mod = 0)
        except:
            self.snd_cli("CN", "close",  self.tp.retime(3), mod = 0)
            self.base.press_x('frm_chat', mod = 1)


    def wad_logout(self):
        self.sleep_timer(self.time_to_sleep)
        self.snd_cli("C", "i.icon-dropdown-arrow.usermenu__icon", 5 * self.tp.retime(1))
        self.snd_cli("I", "gui.frm_main.user_menu#tabmenu-logout", 5 * self.tp.retime(1))
 
    

    def logout(self):
        self.options_menu()
        self.snd_cli('X', '//*[@class="big ico logout color2 end"]',  self.tp.retime(3))
        self.logger.debug(self.msid + "finished")
