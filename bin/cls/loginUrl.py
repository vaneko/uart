from setupDriver import *
import random

class loginUrl(setupDriver):
    """go to URL"""
    @retry(stop_max_attempt_number=333)
    def login(self, URL):
        print "loginurl"
        self.URL = URL
        self.d.get(self.URL)
        while self.d.current_url != self.url:
            time.sleep(1)
            self.d.get(self.URL)
            
        self.logger.debug(self.msid + "loading " + self.URL)

    def fetch_lang(self, mod):
        
        self.mod = mod
        self.logger.debug(self.msid + "fetching languages")
        self.languages = []
        lngs = WebDriverWait(self.d, self.tp.retime(3)).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="content"]/main/form/div/div/section[1]/div/div/label' )))
        lngs.click()
        clmn_id = ['//*[@id="content"]/div[2]/ul[1]', '//*[@id="content"]/div[2]/ul[2]', '//*[@id="content"]/div[2]/ul[3]']
        for i in clmn_id:
            elements = WebDriverWait(self.d, self.tp.retime(3) ).until(
                EC.presence_of_all_elements_located((By.XPATH, i))
            )
            for element in elements:
                if len(element.text.split('\n')) > 1:
                    self.languages += element.text.split('\n')
                else:
                    print element.text.split('\n')
        self.logger.debug(self.msid + "fetch completed")
        if self.mod == 'All':
            self.logger.debug(self.msid + "mode: All")
            self.lng = random.choice(self.languages).encode('utf-8')
        elif self.mod == 'rnd':
            self.lng = random.choice(self.languages).encode('utf-8')
        else:
            self.lng = self.mod
        lngs = WebDriverWait(self.d, self.tp.retime(3)).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="content"]/main/form/div/div/section[1]/div/div/label' )))
        lngs.click()
        strng = "//*[contains(text()," + "'" + self.lng + "')]"
        try:
            self.d.find_element_by_xpath(strng).click()
        except:
            self.d.find_element_by_xpath(strng).click()

        self.logger.debug(self.msid + "language set: " + (self.lng))
        self.logger.debug(self.msid + "language selection finished")