from setupDriver import *
from logger import *
from autoTest import *
import uuid

class testProcessor(setupDriver):
    def __init__(self, host, actions, users, **kwargs):
        self.inurl = None
        self.ssid = None
        self.host = host
        self.actions = actions
        self.users = users
        self.id = ' [' + str(uuid.uuid4())[:8] +'] '
        for key, value in kwargs.items():
            setattr(self, key, value)

    #@retry(stop_max_attempt_number=2)
    def proc_attend(self):
        for u, l in self.users.items():
            try:
                if u is not None:
                    master = u + ':I!c@e2@3w@ar!p;'
                    self.param4 = master + ','.join(x for x in self.users if x is not u)
                    id = ' [' + str(uuid.uuid4())[:8] +'] '
                    self.test2 = autoTest(self.host, self.actions, self.param4, lang = l, msid = id, ini_url = self.inurl, session_id = self.ssid) #mgdectn
                    self.test2.start_exec()
                    self.actions += "O"
                    self.logger.debug(self.test2.msid + 'ended')
                    time.sleep(3)
            except:
                print("ERROR")

        self.logger.debug(self.id + 'for loop finished')
        return