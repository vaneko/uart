#Avatar Generator - www.101computing.net/avatar-generator ...thanks
import random
import time
import turtle



class claudeMonet():
      def __init__(self):
            """ claude likes painting, give him a turtle """
            self.myPen = turtle.Turtle()
            self.myPen.shape("turtle")
            self.myPen.speed(500)
            self.window = turtle.Screen()
            self.window.bgcolor("#69C5FF")

            

            self.skinColors = ["#FCD09D","#FAC47A", "#DBA965", "#CC9D5E","#A3855B","#806645","#665135", "#15A315","#489CF0" ]
            self.eyeColors = ["#489CF0","#15A315", "#8C3303"]
            self.hairColors = ["#000000","#8C3303","#F5D907","#580000","#E31E00","#636363","#FFFFFF"]

            self.myPen.penup()
            self.myPen.goto(0,-100)

            #skinColorIndex=int(input("Chose a skin complexion from 1 (pale) to 7 (dark skin)?"))-1
            #eyeColorIndex=int(input("Color of the eyes? Chose between 1 - blue, 2 - green, 3 - brown."))-1
            #hairColorIndex=int(input("Hair color? Chose between 1 - black, 2 - brown, 3 - Blond, 4 - Auburn, 5 - Red, 6 - Grey, 7 - White."))-1

            self.skinColorIndex = random.randint(0,8)
            self.eyeColorIndex = random.randint(0,2)
            self.hairColorIndex = random.randint(0,6)

      def drawHair(self):
            #Drawing the hair
            self.myPen.penup()
            self.myPen.goto(-50,-50)
            self.myPen.pendown()
            self.myPen.color(self.hairColors[self.hairColorIndex])
            self.myPen.fillcolor(self.hairColors[self.hairColorIndex])
            self.myPen.begin_fill()
            self.myPen.goto(-160,20+random.randint(-10,10))
            self.myPen.goto(-140,110+random.randint(-10,10))
            self.myPen.goto(-130,100+random.randint(-10,10))
            self.myPen.goto(-110,160+random.randint(-10,10))
            self.myPen.goto(-90,150+random.randint(-10,10))
            self.myPen.goto(-70,180+random.randint(-10,10))
            self.myPen.goto(-50,160+random.randint(-10,10))
            self.myPen.goto(-40,190+random.randint(-10,10))
            self.myPen.goto(-10,160+random.randint(-10,10))
            self.myPen.goto(20,180+random.randint(-10,10))
            self.myPen.goto(50,160+random.randint(-10,10))
            self.myPen.goto(70,150+random.randint(-10,10))
            self.myPen.goto(90,160+random.randint(-10,10))
            self.myPen.goto(110,140+random.randint(-10,10))
            self.myPen.goto(130,170+random.randint(-10,10))
            self.myPen.goto(140,110+random.randint(-10,10))
            self.myPen.goto(160,20+random.randint(-10,10))
            self.myPen.goto(-50,-50)
            self.myPen.end_fill()

      def drawFace(self):
            #Drawing the face
            self.myPen.penup()
            self.myPen.goto(0,-150)
            self.myPen.color(self.skinColors[self.skinColorIndex])
            self.myPen.fillcolor(self.skinColors[self.skinColorIndex])
            self.myPen.begin_fill()
            self.myPen.circle(150)
            self.myPen.end_fill()

      def drawSmile(self):
            #Drawing the smile
            self.myPen.penup()
            self.myPen.goto(0,-100)
            self.myPen.color("#F00070")
            self.myPen.fillcolor("#F00070")
            self.myPen.begin_fill()
            self.myPen.circle(70)
            self.myPen.end_fill()  

            self.myPen.penup()
            self.myPen.goto(0,-85)
            self.myPen.color(self.skinColors[self.skinColorIndex])
            self.myPen.fillcolor(self.skinColors[self.skinColorIndex])
            self.myPen.begin_fill()
            self.myPen.circle(80)
            self.myPen.end_fill()

      def drawEyeBrow(self, x):
            #Drawing one eyeBrow at position x
            self.myPen.penup()
            self.myPen.goto(x,50)
            self.myPen.color(self.hairColors[self.hairColorIndex])
            self.myPen.fillcolor(self.hairColors[self.hairColorIndex])
            self.myPen.begin_fill()
            self.myPen.circle(30)
            self.myPen.end_fill()

            self.myPen.penup()
            self.myPen.goto(x,20)
            self.myPen.color(self.skinColors[self.skinColorIndex])
            self.myPen.fillcolor(self.skinColors[self.skinColorIndex])
            self.myPen.begin_fill()
            self.myPen.circle(40)
            self.myPen.end_fill()

      def drawEye(self, x):
            #Drawing one eyeBrow at position x
            self.myPen.penup()
            self.myPen.goto(x,20)
            self.myPen.color("#000000")
            self.myPen.fillcolor("#FFFFFF")
            self.myPen.begin_fill()
            self.myPen.circle(30)
            self.myPen.end_fill()

            self.myPen.penup()
            self.myPen.goto(x,30)
            self.myPen.color(self.eyeColors[self.eyeColorIndex])
            self.myPen.fillcolor(self.eyeColors[self.eyeColorIndex])
            self.myPen.begin_fill()
            self.myPen.circle(20)
            self.myPen.end_fill()

            self.myPen.penup()
            self.myPen.goto(x,40)
            self.myPen.color("#000000")
            self.myPen.fillcolor("#000000")
            self.myPen.begin_fill()
            self.myPen.circle(10)
            self.myPen.end_fill()


      def close_win(self):
            self.window.bye()