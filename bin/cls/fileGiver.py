from locatEle import *
from phpChecker import *
from word_pick import *

class fileGiver(locatEle):

    def __init__(self, driver):
        self.d = driver
        self.fail = 0

    def attachment(self, mod=0, **kwargs):
        """ mod > 0 for mentions """
        
        att_startTime = time.time()
        self.logger.debug(self.msid + "adding attachment")
        self.snd_cli("I", "gui.insert_item.filter#F", 3 * self.tp.retime(2))
        if float(str(self.vers)[:2]) == 12 and float(str(self.vers)[3:][:3]) < 1.2 or 'w' in self.act:
            self.snd_cli("C", "div.ico.ico_f",  self.tp.retime(3), mod=0) #.default
        self.sleep_timer(self.time_to_sleep)
        try:
            self.snd_cli("I", "gui.insert_item.datagrid#EVN_MODIFIED", self.tp.retime(2), mod = 0)
            self.sleep_timer(2*self.time_to_sleep)
            clmn = self.ret_ele("X", "//*[starts-with(@id, 'gui.insert_item.datagrid') and contains(@style, 'top: 0px; height: 33px;')]", 0, self.tp.retime(4)) #and ends-with(@id='*EVNFILENAME')]", 2,  self.tp.retime(2))
            clmn.click()
        
            self.sleep_timer(self.time_to_sleep)
            self.base.press_ok("insert_item")
              
            self.sleep_timer(self.time_to_sleep)
            at_time = str(time.time() - att_startTime)
            if mod > 0: 
                
                ment = kwargs.get("user").split('@')[0]
                self.sleep_timer(self.time_to_sleep)
                txt = "To @[" + str(ment) + "] from " + word_pick(text = self.lng).gime_som()
                self.snd_key("I", "gui.chat_upload.input_desc.input#main", txt,  self.tp.retime(2))
                self.sleep_timer(self.time_to_sleep)
                self.base.press_ok("chat_upload")
                self.logger.debug(self.msid + "tc file added")    
        except:
            self.fail += 1
            at_time = str(time.time() - att_startTime)
            self.logger.error(self.msid + "attachment not found")
            self.logger.error(self.msid + "attach failure " + str(self.fail) )
            self.paparazzi_style('attachment', mod = 0)
            a = phpChecker(self.redrive())
            a.checkphp(self.tp.retime(1))
            self.paparazzi_style(a.phpstat, mod = a.pap)
            self.base.press_ok("insert_item")




        self.logger.debug(self.msid + "attachment duration: " + at_time)
        self.atc_tim.append(at_time)            