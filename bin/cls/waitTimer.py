import time

class waitTimer():
    def __init__(self, speed = 'F'):
        """ init S for slow """
        self.speed = speed
        self.times = {}
        self.times[0] = 0.1
        self.times[1] = 1
        self.times[2] = 10
        self.times[3] = 20
        self.times[4] = 120

    def retime(self, tvar):
        """ tvar = int 
        0: 0.1, 
        1: 1, 
        2: 10, 
        3: 20, 
        4: 120
        
        speed v = default, S = 3 * v """
        if self.speed == 'S':
            return 3 * self.times[tvar]
        else:
            return self.times[tvar]