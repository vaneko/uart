from locatEle import *
from setupDriver import *
from loginUrl import *
from locatEle import *


class phpChecker(locatEle):
    def __init__(self,driver):
        self.d = driver

    def checkphp(self, wait_time):
        phpdown = self.return_multi_elem("I", "gui.connection.btn_re#main", "I", "gui.connection.btn_ok#main",  wait_time)
        try:
            sessdown = phpdown[0]
            if sessdown == "gui.connection.btn_re#main":
                self.snd_cli("I", "gui.conngection.btn_cancel#main", 20, mod = 2)
                self.phpstat = 'SESSION_TIMEOUT'
            else:
                self.snd_cli("I", "gui.connection.btn_ok#main", 20, mod = 1)
                self.phpstat = 'CONNECTION_DOWN'
        except:
            self.phpstat = 'UNKNOWN_ERROR'
        if self.phpstat == "CONNECTION_DOWN":
            self.pap = 2
        else:
            self.pap = 1                
                