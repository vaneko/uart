
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from retrying import retry
from selenium.webdriver.chrome import service
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.common.exceptions import *
import sys
import logging
import os
from logger import *
import time
from datetime import datetime, timedelta
from waitTimer import *

class setupDriver(object):
    """set browser property with script parametrs"""
    def retry_ifno_element(result):
        time.sleep(0.1)
        return len(str(result)) <= 0  

    @retry(retry_on_result=retry_ifno_element)    
    def setBro(self, act, msid):
        self.msid = msid
        self.act = act
        self.drive_det = []
        
        if 'S' in self.act:
            self.tp = waitTimer('S')
        else:
            self.tp = waitTimer()
            



        if 'G' in self.act:
            
            driver_loc = os.path.abspath('chromedriver')
            options = webdriver.ChromeOptions()
            options.add_argument("--start-maximized")
            options.add_argument('use-fake-device-for-media-stream')
            options.add_argument('use-fake-ui-for-media-stream')
            options.add_argument("--verbose");
            self.d = webdriver.Chrome(executable_path=driver_loc, chrome_options=options)
        
        
        elif 'F' in self.act:
            caps = DesiredCapabilities.FIREFOX.copy()
            caps['acceptInsecureCerts'] = True
            self.d = webdriver.Firefox(capabilities=caps)
            
        
        
        elif 'P' in self.act:
            
            options = webdriver.ChromeOptions()
            options.binary_location = "/usr/bin/opera"
            self.d = webdriver.Opera(options=options)
        
        if 'r' not in self.act:
            self.ini_url = self.d.command_executor._url
            self.drive_det.append(self.ini_url)
            self.session_id = str(self.d.session_id)

            self.drive_det.append(str(self.session_id))
        self.logger = Logger(self.msid)
        self.logger.debug(msid + "iniurl ="+ self.ini_url)
        self.logger.debug(msid + "session_id ="+self.session_id)
        if 'P' in self.act:
            self.brva = 'opera'
        else:
            self.brva = self.d.capabilities['browserName']
        print('done')

    #def remo_drive(self, act, msid):
    #    self.msid = msid
    #    self.act = act
    #    self.drive_det = []

    #    if 'F' in self.act:
    #        self.d = webdriver.Remote( 
    #            command_executor = 'http://185.138.223.86:4444/wd/hub',
    #            desired_capabilities={'browserName' : 'firefox', 
    #                                'platform':'LINUX'})
    #    elif 'G' in self.act:
    #        self.d = webdriver.Remote( 
    #            command_executor = 'http://185.138.223.86:4444/wd/hub',
    #            desired_capabilities={'browserName' : 'chrome', 
    #                                'platform':'LINUX'})

    #    self.logger = Logger(self.msid)
    #    self.logger.debug(msid + "iniurl ="+ self.ini_url)
    #    self.logger.debug(msid + "session_id ="+self.session_id)

    #    self.ini_url = self.d.command_executor._url
    #    self.drive_det.append(self.ini_url)
    #    self.session_id = str(self.d.session_id)
                                    
    #    if 'P' in self.act:
    #        self.brva = 'opera'
    #    else:
    #        self.brva = self.d.capabilities['browserName']
    #    print('done')


    def re_use(self, url, sessid, **kwargs):
        self.drive_det = []
        self.url = url
        for key, value in kwargs.items():
            setattr(self, key, value)

        self.logger = Logger(self.msid)
        time.sleep(1)
        self.logger.debug("inheriting driver")
        if 'P' in self.act:
            options = webdriver.ChromeOptions()
            options.binary_location = "/usr/bin/opera"
            caps = webdriver.DesiredCapabilities().OPERA
            caps['binary'] = "/usr/bin/operadriver"
            self.d = webdriver.Remote(command_executor=self.url, desired_capabilities=caps, executable_path='/usr/bin/opera')
        else:
            self.d = webdriver.Remote(command_executor=self.url, desired_capabilities={})
        
        self.d.session_id = sessid
        self.drive_det.append(self.url)
        self.drive_det.append(str(self.d.session_id))
        
        self.logger.debug(self.msid + "driver reused")

    def take_pic(self, host):
        self.host = host
        if 'r' in self.act:
            self.d.get_screenshot_as_file(self.host + self.msid + '.png')
        else:
            self.d.save_screenshot(self.host + self.msid + '.png')
        self.logger.debug(self.msid + 'screenshot saved')

    def quiBro(self):
        try:
            self.d.quit()
        except:
            pass

    def my_handler(self, type, value, tb):
        self.logger.exception("Uncaught exception: {0}".format(self.msid + str(value).decode('utf-8')))
