import wget
import random
import os
import time
import random
import tempfile
import uuid
from robohash import Robohash


class getProfilepic():
    """ profile pic generator """
    def __init__(self, sex='random'):
        #NO sex due GDPR :(
        sex_opt = ['women', 'men']
        if sex == 'random':
            self.sex = random.choice(sex_opt)
        else:
            self.sex = sex
            
        self.pic = ''
        self.name = str(uuid.uuid4())[:8]

    def get_pic(self, mod):
        if int(mod) < 1:
            print("Hurray")
            i = random.randint(0, 99)
            url = 'https://randomuser.me/api/portraits/' + self.sex + '/' + str(i) + '.jpg'
            wget.download(url)
            time.sleep(0.1)
            nameSav = str(i) + '.jpg'
        else:
            #bloodie GDPR!!!
            nameSav = str(self.name) + ".png"
            hash = str(self.name)
            rh = Robohash(hash)
            rh.assemble(roboset='any')
            with open(nameSav, "w") as f:
                rh.img.save(f, format="png")        

        self.pic = os.path.abspath(nameSav)
         