import ConfigParser
import os

class settSetter():
    def __init__(self):
        self.config = ConfigParser.RawConfigParser()

    def load_settings(self):
        self.config.read("settings.conf")
        self.smtp_from = self.config.get('REPORTER', 'SMTP_FROM')
        self.smtp_pass = self.config.get('REPORTER', 'SMTP_PASS')
        self.rcpt_to = self.config.get('REPORTER', 'RCPT_TO')
        self.smtp_server = self.config.get('REPORTER', 'SMTP_SERVER')
        self.smtp_port = self.config.get('REPORTER', 'SMTP_PORT')
        self.USER_PWD = self.config.get('ACCOUNTS', 'USER_PWD')

    def set_settings(self):
        self.config.add_section('REPORTER')
        self.config.set('REPORTER', 'SMTP_FROM', 'reports@ofiko-test.com')
        self.config.set('REPORTER', 'SMTP_PASS', 'wdVdspi1')
        self.config.set('REPORTER', 'RCPT_TO', 'reports@ofiko-test.com')
        self.config.set('REPORTER', 'SMTP_SERVER', '10.16.8.99')
        self.config.set('REPORTER', 'SMTP_PORT', '25')
        self.config.add_section('ACCOUNTS') 
        self.config.set('ACCOUNTS', 'USER_PWD', 'I!c@e2@3w@ar!p')


        with open('settings.conf', 'w+') as configfile:
            self.config.write(configfile)