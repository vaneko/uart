# Python TCP Client A
import socket 
import sys

status = sys.argv[1]
 
host = '10.16.8.99'
port = 2107

BUFFER_SIZE = 2000 
MESSAGE = status
 
tcpClientA = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
tcpClientA.connect((host, port))
 
tcpClientA.send(MESSAGE)     
 
tcpClientA.close() 
